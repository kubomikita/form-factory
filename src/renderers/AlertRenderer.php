<?php

namespace Kubomikita\Form\Response;

use Kubomikita\Form;

class Alert extends Base implements IResponse {
	
	public function render() {
		$ret = $this->getHolder("start");
		$ret .= '<div class="d-flex align-items-center">';
		if($this->image != null) {
			$ret .= '<div style="font-size:120%;">'.$this->image.'</div>';
			$ret .= '<div class="ml-3">';
		} else {
			$ret .= '<div>';
		}

		if($this->title != null) {
			$ret .= '<div><strong>'.$this->getTitle().'</strong></div>';
		}
		if($this->message != null) {
			$ret .= '<div>'.$this->getMessage().'</div>';
		}
		$ret .= '</div>';
		$ret .= '</div>';
		$ret .= $this->getHolder("end");
		$ret .= $this->getJavascript();
		return $ret;
	}
}