<?php

namespace Kubomikita\Form\Response;

use Kubomikita\Form;

interface IResponse {
	public function setForm(Form $form);
	public function render();
}