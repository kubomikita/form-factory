<?php
namespace Kubomikita\Form\Response;

use Kubomikita\Form;
use Nette\SmartObject;

class Base implements IResponse {
	const NOT_IMPLEMENTED = 'Form/Response is not implemented yet.';
	const IMG_DANGER = '<i class="fas fa-exclamation-triangle"></i>';
	const IMG_SUCCESS = '<i class="fas fa-check"></i>';
	use SmartObject;
	/** @var Form */
	private $form;
	public $title = '';
	public $message = 'Formulár bol úspešne odoslaný.';
	public $image = self::IMG_SUCCESS;
	public $holder = 'div class="alert alert-success"';
	public $holderDanger = 'div class="alert alert-danger"';

	protected $hideForm = false;
	protected $reload = false;
	protected $reloadTime = 2000; //ms
	protected $javascript = [];
	protected $redirect;
	protected $redirectTime = 0;

	public function setForm( Form $form ) {
		$this->form = $form;
		return $this;
	}
	public function render() {
		return static::NOT_IMPLEMENTED;
	}

	public function setHideForm(bool $hide = true){
		$this->hideForm = $hide;
		return $this;
	}
	public function setReload(bool $reload = true, int $time = null){
		$this->reload = $reload;
		if($time !== null) {
			$this->reloadTime = $time;
		}
		return $this;
	}
	public function setDanger(bool $danger = true){
		if($danger){
			$this->image = static::IMG_DANGER;
			$this->holder = $this->holderDanger;
		}
		return $this;
	}
	public function setJavascript(string $javascript, $time = null){
		$this->javascript[(int) $time][] = $javascript;
		return $this;
	}
	public function setRedirect(string $redirect, int $time = null){
		$this->redirect = $redirect;
		if($time !== null) {
			$this->redirectTime = $time;
		}
		return $this;
	}
	public function setScrollTo(string $element, int $position){
		$this->setJavascript('scrollToElement("'.$element.'", '.$position.');');
		return $this;
	}

	protected function getTitle(){
		if($this->form instanceof Form) {
			return $this->form->getTranslator()->translate( $this->title );
		}
		return $this->title;
	}
	protected function getMessage(){
		if($this->form instanceof Form) {
			return $this->form->getTranslator()->translate( $this->message );
		}
		return $this->message;
	}

	protected function getJavascript(){
		$ret = "";
		if($this->hideForm) {
			$ret .= '$("#'.$this->form->getId().'").html(\'\');' . PHP_EOL;
		}
		if($this->reload){
			$ret .= 'setTimeout(function(){ location.reload(); },'.$this->reloadTime.');' . PHP_EOL;
		}
		if(!empty($this->javascript)){
			$ret .= $this->proccessJavascript();
		}
		if($this->redirect){
			if($this->redirectTime > 0) {
				$ret .= 'setTimeout(function(){ window.location.href=\''.$this->redirect.'\'; },' . $this->redirectTime . ');' . PHP_EOL;
			} else {
				$ret .= 'window.location.href=\''.$this->redirect.'\';';
			}

		}

		if(strlen(trim($ret)) > 0 ){
			return '<script>$(document).ready(function(){' . $ret . '})</script>';
		}
		return;
	}
	protected function proccessJavascript(){
		$ret = "";
		foreach($this->javascript as $time =>  $js){
			foreach($js as $j) {
				if ( $time > 0 ) {
					$ret .= 'setTimeout(function(){ ' . $j . ' },' . $time . ');';
				} else {
					$ret .= $j;
				}
			}
		}
		return $ret;
	}
	protected function getHolder($type = "start"){
		if($type == "start"){
			return '<'.$this->holder.'>';
		} elseif($type = "end"){
			$e = explode(" ",$this->holder);
			return '</'.$e[0].'>';
		}
	}
}