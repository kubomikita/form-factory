<?php


namespace Kubomikita;

use Kubomikita\Commerce\IConfigurator;
use Kubomikita\Commerce\IDiConfigurator;
use Kubomikita\Form\Renderer\IRenderer;
use Kubomikita\Form\Response\Alert;
use Kubomikita\Form\Response\IResponse;
use Kubomikita\UI\Component;
use Nette\Application\AbortException;
use Nette\Application\BadRequestException;
use Nette\InvalidStateException;
use Nette\Utils\ArrayHash;
use Nette\Utils\Callback;
use Nette\Utils\Strings;

/**
 * @property-read IDiConfigurator $context
 */
class Form implements \ArrayAccess {
	const PATTERN_EMAIL = "^\s*[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+\s*$";
	const PATTERN_ICO = "^[0-9]{8,8}$";
	const PATTERN_MOBIL_SK = '^\s*[9][0-9]{8}\s*$'; //[+][4][2][1]
	const PATTERN_MOBIL = '^\s*[0-9]{8,}\s*$'; //[+]
	const PATTERN_MOBIL_ADMIN = '^[+][0-9]{11,}$'; //
	const PATTERN_PSC_INTER = "^[a-zA-Z0-9]{4,10}$";
	const PATTERN_PSC = "^[0-9]{5}$";
	const PATTERN_PREDVOLBA = "^\+[\d]{2,4}$";
	const PATTERN_STREET = "^[0-9-\/\s]+$";
	const PATTERN_STREET_FIRSTDAY = "^[0-9]+[\/0-9a-zA-Z-]*$";
	const PATTERN_ICDPH = '^([A-Z]+)([0-9]{10})$';
	//const PATTERN_ALPHANUMERIC = "^[0-9\pL\s\.-]+$";
	//const PATTERN_ALPHANUMERIC = "^(.+)$";
	//const PATTERN_ALPHANUMERIC = "^[\pL\pN.\s-]+$";
	const PATTERN_ALPHANUMERIC = "^[\pL\pN.\s-\/\.\,\+\&\;\:]+$";
	const PATTERN_ALPHANUMERIC_73 = "^[\pL\pN.\s\-\/\.\,\+\&\;\:]+$";
	const PATTERN_ADDRESS = "^[\pL\pN.\s\-\/\.\,\+\&\;\:]+$";
	const PATTERN_NUMERIC = "^[0-9]+$";


	const CsrfTokenName = "token";
	const CsrfTokenLifeTime = 6;

	/** @var IDiConfigurator */
	//public $context;

	/** @var callable[]  function (Form $sender); Occurs when the form is submitted and successfully validated */
	public $onSuccess;

	private $blocks = array();
	// nazov formulara
	private $name;
	private $isajax;
	private $ajax = false;
	private $ajax_scroll = false;
	private $success_function;
	private $success_function_callable;
	private $error_function;
	//true ak vsetky povinne polozky su vyplnene, inak false
	private $valid = true;
	private $formClass;
	private $method = "POST";
	private $token = "";
	private $protection = false;
	private $id;
	private $class;

	static $instances = array();

	/** @var FormFactory */
	private $formFactory;
	/** @var IFormTranslator */
	private $translator;

	private $valuesSended = [];
	private $values = [];

	/** @var IFormAlertHandler  */
	private $alertHandler = null;
	private $alertElementStart = "div";
	private $alertElementEnd = "div";
	private $alertContainerElementStart = 'div class="alert alert-danger"';
	private $alertContainerElementEnd = 'div';

	private $debugMode = false;
	private $strictMode = false;
	private $noescapeMode = false;

	private $req = [];

	private $rendered = [];

	private $nospam = true;
	public $parent=[];
	public $caller;


	/** @var array */
	protected $multilang = [];

	public function __get( $name ) {
		if($name == "context"){
			try {
				return Service::get( "configurator" );
			} catch (\Exception $e){
				bdump($e,"get form");
				return Service::get("context");
			}
		}
	}

	/**
	 * @param array $multilang
	 */
	public function setMultilang( array $multilang ) {
		$this->multilang = $multilang;
	}

	/**
	 * @return array
	 */
	public function getMultilang(): array {
		return $this->multilang;
	}

	public function setCaller(array $backtrace){
		foreach($backtrace as $b){
			if(isset($b["object"]) && $b["object"] instanceof FormAjaxHandler){
				$this->caller = $b["object"];
				break;
			}
		}
	}
	public function getCaller(){
		return $this->caller;
	}
	public function getErrors(){
		return $this->req;
	}

	/**
	 * Set debug mode on
	 */
	public function setDebugMode(){
		$this->debugMode = true;
	}

	/**
	 * Set strict mode on
	 */
	public function setStrict(){
		$this->strictMode = true;
	}

	/**
	 * Checks if strict mode on
	 * @return bool
	 */
	public function isStrict(){
		return (bool) $this->strictMode;
	}

	public function setAlertElement($element){
		$e = explode(" ",$element);
		$el = reset($e);
		$this->alertElementStart = $element;
		$this->alertElementEnd = $el;
		return $this;
	}
	public function setAlertContainerElement($element){
		$e = explode(" ",$element);
		$el = reset($e);
		$this->alertContainerElementStart = $element;
		$this->alertContainerElementEnd = $el;
		return $this;
	}
	public function setAlertHandler($handler){
		$this->alertHandler = $handler;
		return $this;
	}
	public function __construct($name = null) {
		if(!FormFactory::$registered){
			throw new \Exception("FormFactory not initialized.");
		}
		if(!isset($_SESSION[FormFactory::GENERATOR]["form-factory"]) and !($_SESSION[FormFactory::GENERATOR]["form-factory"] instanceof FormFactory)){
			throw new \Exception("FormFactory not initialized.");
		}
		$this->formFactory = $_SESSION[FormFactory::GENERATOR]["form-factory"];
		if($this->formFactory->getAlertHandler() !== null){
			$this->setAlertHandler($this->formFactory->getAlertHandler());
		}

		$this->setMultilang($this->formFactory->getMultilang());

		$this->name = $name; //Strings::webalize($name);
		if ($this->name == null) {
			$this->name = self::generateName("form-factory");
		}
		$this->id = Strings::webalize($this->name);
		self::$instances[$this->id] = $this;
		$this->getTranslator()->setForm($this);
		$backtrace=debug_backtrace();
		$this->setCaller($backtrace);
		//if(is_callable([$backtrace[1]["object"],$backtrace[1]["function"]])) {
			//$this->parent["function"] = $backtrace[1]["function"];
			//$this->parent["class"]    = $backtrace[1]["class"];

			//$this->setSession("parent",$this->parent);
			/*dump( $this->parent, $backtrace,$_SESSION[FormFactory::GENERATOR] );*/
		//}
		//bdump($backtrace,"FORM ".$name." Backtrace");
		//bdump($this,"FORM ".$name." after INIT");


	}

	public static function getComponentName(string $formId){
		return [
			"createComponent".Strings::firstUpper($formId)."",
			"createComponent".Strings::firstUpper($formId)."Form",
		];
	}


	/**
	 * @param IFormTranslator $translator
	 *
	 * @return Form
	 */
	public function setTranslator(IFormTranslator $translator): self{
		$this->translator = $translator;
		$this->translator->setForm($this);
		return $this;
	}

	/**
	 * @return IFormTranslator
	 */
	public function getTranslator(){
		if($this->translator === null){
			return $this->getFormFactory()->getTranslator();
		}
		return $this->translator;
	}

	public function setDefaults(array $defaults, $silent = true){
		$items = $this->getItems(true);
		//dumpe($defaults,$items);
		foreach($defaults as $key => $value){
			if(isset($items[$key])){
				/** @var FormItem $item */
				$item = $items[$key];
				if($item instanceof FormItemCheckbox) {
					if ((int) $value) {
						$item->setChecked();
					}
				} elseif ($item instanceof FormItemRadio){
					//if ((int) $value) {
						$item->setChecked($value);
					//}
				} elseif ($item instanceof FormItemSelect) {
					$item->setSelected($value);
				} else {
					$item->setValue(FormFactory::quote($value, $this->noescapeMode));
				}

			} else {
				if(!$silent) {
					trigger_error( "While setting default values, an error occured. FormItem with key '$key' not exists." );
				}
			}
		}
	}
	public function setValues(array $values){
		foreach($values as $k=>$v){
			if(is_string($v)){
				$this->valuesSended[$k] = FormFactory::quote($v, $this->noescapeMode);
			} else {
				$this->valuesSended[$k] = $v;
			}
		}

	}
	public function setRendered(array $values){
		if(empty($this->rendered)) {
			foreach ( $values as $k => $v ) {
				$item                 = $this->getFormItemByName( $k );
				$this->rendered[ $k ] = $item;
			}
		}
	}
	public function setSession($key,$val){
		if(!isset($_SESSION[FormFactory::GENERATOR]["Forms"][$this->getId()][$key])){
			if(is_array($val)){
				$_SESSION[FormFactory::GENERATOR]["Forms"][$this->getId()][$key] = [];
			} else {
				$_SESSION[FormFactory::GENERATOR]["Forms"][$this->getId()][$key] = null;
			}
		}
		$_SESSION[FormFactory::GENERATOR]["Forms"][$this->getId()][$key] = (is_array($val))?($val + $_SESSION[FormFactory::GENERATOR]["Forms"][$this->getId()][$key]):$val;
	}
	public function getSession($key = null){
		if($key === null){
			return $_SESSION[FormFactory::GENERATOR]["Forms"][$this->getId()];
		}
		return $_SESSION[FormFactory::GENERATOR]["Forms"][$this->getId()][$key];
	}
	/**
	 * @return FormFactory class
	 */
	public function getFormFactory(){
		return $this->formFactory;
	}

	/**
	 * Get instance of Form
	 * @param $id
	 *
	 * @return Form class
	 */
	public static function getInstance($id){
		/*bdump($id);
		bdump(self::$instances);
		bdump($_SESSION[FormFactory::GENERATOR]["Forms"]);*/

		if(isset(self::$instances[$id])){
			return self::$instances[$id];
		} else {
			if(isset($_SESSION[FormFactory::GENERATOR]["Forms"][$id]["object"])){
				return unserialize($_SESSION[FormFactory::GENERATOR]["Forms"][$id]["object"]);
			}
		}
		throw new FormException("Instance with id '$id' not exists.");
		return null;
	}
	public function addProtection(){
		if(!$this->ajax){
			$this->protection = true;
			if(!$_POST){
				$this->setToken();
			}
		}
		return $this;
	}
	private function setToken(){
		$this->token=$this->generateToken();
		$_SESSION[self::CsrfTokenName][$this->id] = $this->token;
	}
	private function checkToken($token,$token_time){
		$session_token = $_SESSION[self::CsrfTokenName][$this->id];
		if(time() > ($token_time + self::CsrfTokenLifeTime)){
			$this->addError("Platnosť CSRF tokena vypršala, odošlite formulár znovu.");
			return false;
		}
		if($token != $session_token or $token == ""){
			$add="";
			if($token != ""){
				$add = "Refresh tejto stránky je zakázaný, odošlite formulár kliknutím na odosielacie tlačidlo!";
			}
			$this->addError("Neplatný CSRF token. $add");
			return false;
		}

		return true;
	}
	private function generateToken() {
		return sha1(uniqid(md5($this->id)));//rand(1, 1e9);
	}
	public static function getForms(){
		return self::$instances;
	}
	/**
	 * Generate form name if not set
	 * @param $i
	 *
	 * @return string
	 */
	private static function generateName($i) {
		if (count(self::$instances) > 0) {
			$l = strlen(count(self::$instances));
			return $i . count(self::$instances);
		}
		return $i;
	}

	/**
	 * Ajaxize form
	 * @param bool $ajax
	 *
	 * @return $this
	 * @throws \Exception
	 */
	public function setAjax($ajax=true){
		if(!($this->getFormFactory()->getAjaxHandler() instanceof IFromAjaxHandler)){
			throw new \Exception("Unable to ajaxize from. Ajax handler not defined.");
		}
		$this->ajax=$ajax;
		$this->protection = false;
		return $this;
	}

	/**
	 * Set if is ajax call
	 * @param $val
	 */
	public function setIsAjax($val){
		$this->isajax = $val;
	}

	/**
	 * Get if ajax turned on
	 * @return bool
	 */
	public function getAjax(){
		return $this->ajax;
	}

	/**
	 * Returns form id
	 * @return string
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * Returns form id
	 * @return string
	 */
	public function getFormID() {
		return $this->id;
	}


	public function setMethod($method) {
		$this->method = $method;
		return $this;
	}

	public function setName($n){
		$this->name = $n;
		return $this;
	}

	public function setClass($f) {
		$this->class = $f;
		return $this;
	}
	public function setAjaxScrollOnSuccess($t){
		$this->ajax_scroll = $t;
		return $this;
	}
	public function setFormClass($f){
		$this->formClass = $f;
		return $this;
	}
	public function setOnSuccess(callable $func) {
		if(is_string($func)){
			$this->success_function_callable = explode("::",$func);
		} elseif(is_array($func)) {
			$this->success_function_callable = $func;
			if(is_object($func[0])){
				$obj = get_class($func[0]);
				$this->success_function_callable[0] = $obj;
				$function = $obj."::".$func[1];
			} else {
				$function = implode("::",$func);
			}
			$func = $function;
		}

		$this->success_function = $func;
		return $this;
	}
	public function setOnError($func){
		$this->error_function = $func;
		return $this;
	}
	public function countItems(){
		$count = 0;
		foreach ( $this->blocks as $blocks ) {
			 $count += count($blocks->getItems());
		}
		return $count;
	}
	public function addBlock($block) {
		$this->blocks[] = $block;
	}
	public function getBlock() : FormBlock {
		return $this->blocks[0];
	}
	public function register(){
		$this->beforeDraw();
	}
	private function beforeDraw(){
		if($this->onSuccess === null) {
			$this->setSession( "object", serialize( $this ) );
		}
		self::$instances[$this->getId()] = $this;
	}

	public function getLabel($name, $attrs=[]){
		$item = $this->getFormItemByName($name);
		if($item === null){
			throw new \Exception("Control with name '$name' not exists.");
		}
		return $item->getLabel($this->sanitizeAttrs($attrs));
	}
	private function sanitizeAttrs($attrs){
		foreach($attrs as $key =>$value){
			if(is_numeric($key)){
				unset($attrs[$key]);
			}
		}
		return $attrs;
	}
	public function getLabelPart($name,$part,$attrs = []){
		/** @var FormItem $item */
		$item = $this->getFormItemByName($name);
		if($item === null){
			throw new \Exception("Control with name '$name' not exists.");
		}

		return $item->getLabelPart($part,$this->sanitizeAttrs($attrs));
	}
	public function getControl($name,$attrs = []){
		$item = $this->getFormItemByName($name);
		if($item === null){
			throw new \Exception("Control with name '$name' not exists.");
		}
		$this->rendered[$item->getName()] = $item;
		foreach($item->getSiblings() as $sibling){
			$this->rendered[$sibling->getName()] = $sibling;
		}
		$this->beforeDraw();
		return $item->getControl($this->sanitizeAttrs($attrs));
	}
	public function getControlPart($name,$part,$attrs = []){
		/** @var FormItem $item */
		$item = $this->getFormItemByName($name);
		if($item === null){
			throw new \Exception("Control with name '$name' not exists.");
		}
		$this->rendered[$item->getName()] = $item;
		$this->beforeDraw();

		return $item->getControlPart($part,$this->sanitizeAttrs($attrs));
	}
	public function start(){
		$this->beforeDraw();
		$ret = "";
		if($this->ajax != ""){
			$ret .= '<form class="' . $this->formClass . ' '.$this->class.'" method="' . $this->method . '" ';
			$ret .= 'data-generator="'.FormFactory::GENERATOR.'" ';
			$ret .= 'data-func="'.$this->success_function.'" ';
			$ret .= 'data-method="'.$this->method.'" ';
			$ret .= 'id="' . $this->id . '" ';
			$ret .= 'data-ajax="1" ';
			$ret .= 'data-ajax-scroll="'.(int)$this->ajax_scroll.'" ';
			$ret .= 'enctype="multipart/form-data" ';
			$ret .= 'role="form" ';
			if(!empty($this->parent)){
				$ret .= 'data-caller-class="'.$this->parent['class'].'" ';
				$ret .= 'data-caller-function="'.$this->parent["function"].'" ';
			}
			$ret .= '>'; // onsubmit="javascript:kubomikitaAjaxForm(\''.$this->id.'\','.(int)$this->ajax_scroll.');return false;"
			$ret .= '<input type="hidden" name="ajax" value="1">';
			$ret .= '<input type="hidden" name="formId" value="'.$this->id.'">';
			$ret .= '<input type="hidden" name="formName" value="'.$this->name.'">';
			$ret .= '<noscript><div class="alert alert-danger">Pre správne fungovanie formulára si prosím zapnite <strong>JavaScript</strong>.</div></noscript>';
		} else {
			$ret .= '<form class="' . $this->formClass . ' '.$this->class.'" role="form" method="' . $this->method . '" id="' . $this->id . '" enctype="multipart/form-data" role="form">';
		}
		return $ret;
	}

	private function getNoRendered(){
		$ret = [];
		foreach($this->getItems(true) as $item){
			if(!isset($this->rendered[$item->getName()])){
				$ret[] = $item;
			}
		}
		return $ret;
	}

	public function isRendered($controlName){
		dump($controlName,$this->rendered);
		return isset($this->rendered[$controlName]);
	}

	public function end($addSlashes = false){
		$ret = "";
		//dump($this);
		if(empty($this->blocks)){
			throw new InvalidStateException("Form '".$this->getId()."' has no defined blocks.");
		}
		/** @var FormItem class */
		foreach($this->getItems() as $item) {
			if($item->getType() == "hidden" && !isset($this->rendered[$item->getName()])){
				$ret .= $this->getControl($item->getName());
			}
		}

		if(count($this->rendered) != $this->countItems()){
			/** @var IConfigurator $c */
			$c = \Registry::get("container");

			if($c instanceof IConfigurator && $c->isProduction()) {
				throw new \Exception( "Some items missing in template." );
			} else {
				ob_start();
				//dump($this->rendered,$this->countItems(),$this->getItems(true));
				echo '<div class="alert alert-danger">
						<i class="fas fa-exclamation-triangle"></i>
						<strong>Niektoré prvky z formulára sa v šablóne nenachádzajú:</strong>';
				/** @var FormItem $item */
				foreach($this->getNoRendered() as $item){
					echo ' - '.$item->getName()." (".$item->getRawLabel().")<br>";
				}
				echo '</div>';
				$ret .= ob_get_clean();
			}
		}

		$nospam = (($addSlashes)?addslashes($this->drawNospam()):$this->drawNospam());
		//dump($nospam,$this->drawNospam());
		$protec = $this->drawProtection();

		$ret .= $nospam;
		//$ret .= $protec;

		$ret .= '</form>';

		$this->beforeDraw();

		return $ret;
	}
	public function errors(){
		$ret = "";
		$ret .= $this->reactionToSuccess();
		$ret .= '<div id="'.$this->id.'_responseText" class="form-response-div"></div>';
		return $ret;
	}
	/**
	 * Draws whole Form
	 * @return string
	 */
	public function draw() {
		//$this->beforeDraw();


		ob_start();
		$ret = "";
		$ret .= $this->errors();
		$ret .= $this->start();
		$ret .= $this->drawBlocks();
		$ret .= $this->end();
		echo $ret;

		$return = ob_get_clean();

		return $return;
		//return $ret;
	}

	public function render() {
		echo $this->draw();
	}

	/**
	 * Draws all form blocks
	 * @return string
	 */
	private function drawBlocks(){
		$ret = "";
		foreach ($this->blocks as $block) {
			foreach($block->getItems() as $formItem){
				if($formItem->getName() != null){
					$this->values[$formItem->getName()] = $formItem->getType();
					$this->rendered[$formItem->getName()] = $formItem;
				}

			}
			$ret .= $block->draw();
		}
		return $ret;
	}
	private function drawProtection(){
		$ret = "";
		if($this->protection){
			$ret .= '<input type="hidden" name="'.$this->id."_".self::CsrfTokenName.'" value="'.$this->token.'">';
			$ret .= '<input type="hidden" name="'.$this->id."_".self::CsrfTokenName.'_time" value="'.time().'">';
		}
		return $ret;
	}
	private function drawNospam(){
		return "";
		if($this->isajax) {
			//

		}
		return  '
                <noscript><p>Vyplňte nospam: <input name="my_email" size="6" /></p></noscript>
                <script>
                //$("#'.$this->getId().'").append(\'<input type="hidden" name="my_email" value="no\' + \'spam" />\');
                var form = document.getElementById("'.$this->getId().'");
				var input = document.createElement("input");
				input.setAttribute("type","hidden");
				input.setAttribute("name","my_email");
				input.setAttribute("value","no"+"spam");
				form.appendChild(input);
                </script>';
	}

	public function isValid(){
		return (bool) $this->valid;
	}

	private function getSuccessFunctionCallable(){
		$callable = $this->success_function_callable;
		if(!is_object($callable[0])){
			$object = $callable[0];
			$callable[0] = $this->context->getService("controller")->createOne($object);
		}
		return $callable;
	}

	/**
	 * Check all form inputs and render errors|success
	 * @return string
	 */
	public function reactionToSuccess(){

		$formId = null;
		if(!$this->isajax && isset($_POST["form_id"])){
			$formId = $_POST["form_id"];
		} elseif($this->isajax && isset($_POST["formId"])){
			$formId = $_POST["formId"];
		}

		if($formId == $this->getId()) {
			if ( $this->success_function != null) {
				ob_start();

				if ( ! $this->isajax ) {
					//dump($this,$_POST);
					//exit;
					if ( ! isset( $_POST["my_email"] ) or $_POST['my_email'] != 'nospam' ) {
						//return;
					}
					$this->setValues($_POST + $_FILES);
					unset( $this->valuesSended["form_id"] );
					unset( $this->valuesSended["my_email"] );
				} else {
					$this->valid = false;
					if ( ! empty( $this->valuesSended ) ) {
						$_POST       = $this->valuesSended;
						$this->valid = true;
					}
				}
				$req = [];
				foreach ( $this->blocks as $blocks ) {
					/** @var $item FormItem */
					foreach ( $blocks->getItems() as $item ) {
						$this->checkFormItem($item,$_POST);
					}
				}

				$req = $this->req;


				if ( $this->protection ) {
					$token_name = $this->id . "_" . self::CsrfTokenName;
					//if($_POST[$token_name]){
					dump($_POST);
					$compare = $this->checkToken( $_POST[ $token_name ], $_POST[ $token_name . "_time" ] );
					unset( $_POST[ $token_name ] );
					unset( $_POST[ $token_name . "_time" ] );
					dump($compare,$_POST[ $token_name ] ,$_POST[ $token_name."_time" ] );
					$this->setToken();
					if ( $compare === false ) {
						return "";
					}
				}
				if($this->debugMode){
					echo '<pre>Debug Mode';
					dump($this);
					echo '</pre>';
				}
				//exit;
				if ( $this->valid ) {
					unset( $_POST["form_id"] );
					unset( $_POST["my_email"] );

					if ( FormFactory::function_exists( $this->success_function ) || is_callable($this->success_function_callable) ) {
						$this->resetErrors();
						try {

							if($this->onSuccess !== null) {
								if (!is_array($this->onSuccess) && !$this->onStart instanceof \Traversable) {
									throw new Nette\UnexpectedValueException('Property Form::$onSuccess must be array or Traversable, ' . gettype($this->onSuccess) . ' given.');
								}
								foreach($this->onSuccess as $handler){
									$params = Callback::toReflection($handler)->getParameters();
									$values = isset($params[1]) ? $this->getValues($params[1]->isArray()) : null;
									//Callback::invoke($handler, $this, $values);
									$handler($this,$values);
								}

							} else {
								$callable = $this->getSuccessFunctionCallable();
								$loadComponents = [$callable[0], "onStartup"];
								$loadComponents();
								$callable($this, $this->getValues(false));
								//Callback::invoke([$callable[0], "onStartup"]);
								//Callback::invoke( $callable, $this );
							}
							//call_user_func( $this->success_function, $this );
						} catch(FormException $e){
							$this->renderError([$e->getDomId()=>$e->getMessage()]);
							if(FormFactory::function_exists($this->error_function)) {
								$this->req[$e->getDomId()] = $e->getMessage();
								call_user_func($this->error_function, $this);
							}
						} catch (BadRequestException $e){
							$r = new Alert();
							$r->setForm($this);
							$r->setDanger();
							$r->setHideForm();
							$r->setReload(true, 3000);
							$r->title = $e->getCode() . " ".end(explode("\\",get_class($e)));
							$r->message = $e->getMessage();
							echo $r->render();
						}
					} else {
						return '<div class="alert alert-info">Success function not exists. </div>';
					}
				} else {
					$this->renderError($req);
					if(FormFactory::function_exists($this->error_function)) {
						call_user_func($this->error_function, $this);
					}
				}

				$ob = ob_get_contents();
				ob_end_clean();

				return $ob;
			} else {
				return '<div class="alert alert-info"> Form hasnt got sucess function. </div>';
			}
		}
	}

	/**
	 * @return ArrayHash
	 */
	public function getRawHttpData() : ArrayHash
	{
		return ArrayHash::from($this->valuesSended);
	}

	/**
	 * @param ArrayHash $data
	 *
	 * @return ArrayHash
	 */
	private function cleanRawHttpData(ArrayHash $data) : ArrayHash
	{
		$items = $this->getItems(true);
		$clean = [];
		/*foreach($data as $key => $value){
			if(isset($items[$key])){
				$clean[$key] = $value;
			}
		}*/
		//dump($data);
		//dump($items);
		foreach($items as $key => $FormItem){
			if(!($FormItem instanceof FormItemSubmit)) {
				if ( isset( $data[ $key ] ) ) {
					$value = $data[$key];
					if(( $FormItem instanceof FormItemCheckbox )){
						$value = (int) $data[$key];
					} elseif ($FormItem instanceof FormItemSelect){
						if($data[$key] instanceof ArrayHash){
							if(empty($data[$key])){
								$value = null;
							}
						} else {
							if ( strlen( trim( $data[ $key ] ) ) === 0 ) {
								$value = null;
							}
						}
					}
					//$value = ( $FormItem instanceof FormItemCheckbox ) ? (int) $data[ $key ] : $data[ $key ];
					$clean[ $key ] = $value;
				} elseif ( ( $FormItem instanceof FormItemCheckbox ) ) {
					$clean[ $key ] = 0;
				} else {
					$clean[ $key ] = null;
					//trigger_error("Form component with key '$key' is empty!");
				}
			}
		}

		if(!empty($this->multilang)) {
			$text = [];
			foreach ($clean as $key => $value) {
				if(Strings::contains($key,"txt-")) {
					$match = Strings::match($key,'/[a-z]+-([a-z]{2})-([a-z_]+)/m');
					$text[$match[1]][$match[2]] = $value;
					unset($clean[$key]);
				}
			}
			if(!empty($text)) {
				$clean["txt"] = $text;
			}
		}
		return ArrayHash::from($clean);
	}

	/**
	 * @param bool $formated
	 *
	 * @return ArrayHash
	 */
	public function getValues($formated = true){
		$data = $this->cleanRawHttpData($this->getRawHttpData());
		if($formated){
			$values = [];
			foreach($data as $key => $value){
				$label = ($this->rendered[$key] instanceof FormItem ? strip_tags($this->rendered[$key]->getRawLabel()) : $key);
				$values[$label] = $value;
			}
			return ArrayHash::from($values);
		}
		return $data;
	}

	/*public function getValues($formated = true){
		if(!$formated){
			return $this->valuesSended;
		} else {
			$values = [];
			//dump($this->valuesSended,$this->rendered);
			foreach($this->valuesSended as $key => $value){
				//dump($this->rendered[$key],$this->rendered[$key] instanceof FormItem); //TODO: doriesit toto
				//if($this->rendered[$key] instanceof FormItem) {
					$values[ strip_tags( $this->rendered[ $key ]->getRawLabel() ) ] = $value;
				//}
			}
			return $values;
		}
	}*/
	public function getItems($keyName = false){
		$ret = [];
		foreach ( $this->blocks as $blocks ) {
			/** @var $item FormItem */
			foreach ( $blocks->getItems() as $item ) {
				if($keyName) {
					$ret[ $item->getName() ] = $item;
				} else {
					$ret[ ] = $item;
				}
			}
		}
		return $ret;
	}
	public function getFormItemByName($name){
		foreach ( $this->blocks as $blocks ) {
			/** @var $item FormItem */
			foreach ( $blocks->getItems() as $item ) {
				if($item->getName() == $name){
					return $item;
				}
			}
		}
		return null;
	}
	public function checkFormItem(FormItem $item, $POST){
		if($item->getName() == "robots") {
			//dump( $item, $POST, $item->getName(), $item->getRequired() );
		}
		//echo "<hr>";
		if ( $item->getName() !== null) {
			if($item->getType() === null or $item->getType() == "submit"){
				return;
			}
			if(!isset($POST[$item->getName()])) {
				$POST[$item->getName()] = null;
			}
			if ( $POST && $item->getRequired() && ($POST[ $item->getName() ] === null || trim($POST[ $item->getName() ]) == '') ) {
				$dependsOn = $item->getDependsOn();
				if($dependsOn !== null) {
					if(!isset($POST[$dependsOn])){
						$POST[$dependsOn] = null;
					}
					$itemDependsOn = $item->checkDependsOn( $POST[ $dependsOn ] );
					if ( $dependsOn != null and ! $itemDependsOn ) {
						return;
					}
				}
				$this->req[$item->getErrorIdentifier()] = $item->getRequired(); // . '</div>';
				$this->valid = false;
			}
			if ( isset( $POST[ $item->getName() ] ) ) {
				$ch = FormItem::checkPattern( $item->getPattern(), $POST[ $item->getName() ] );

				if ( $ch !== null ) {
					if ( $ch == false ) {
						$dependsOn = $item->getDependsOn();
						if($dependsOn !== null) {
							if(!isset($POST[$dependsOn])){
								$POST[$dependsOn] = null;
							}
							$itemDependsOn = $item->checkDependsOn( $POST[ $dependsOn ] );
							if ( $dependsOn != null and ! $itemDependsOn ) {
								return;
							}
						}
						if ( $POST[ $item->getName() ] != "" ) {
							$this->req[$item->getErrorIdentifier()] = $item->getPatternText();
							$this->valid = false;
						}
					}
				}
				$item->setValue( $POST[ $item->getName() ] );
			}
			$equal = $item->getEqual();
			if($POST && !empty($equal)){
				if($POST[$item->getName()] !== $POST[$equal[0]]){
					$this->req[$item->getErrorIdentifier()] = $equal[1];
					$this->valid=false;
				}
			}
		}
	}
	/**
	 * Add error in success function
	 * @param $message
	 * @param null $domId
	 *
	 * @throws FormException
	 */
	public function addError($message,$domId=null){
		if($this->valid){
			throw new FormException($this->getTranslator()->translate($message),$domId);
		}
	}

	/**
	 * Return formatted input/item id
	 * @param $item FormItem - >name
	 *
	 * @return string
	 */
	public function getItemId($item){
		return $this->getId()."-".$item;
	}

	/**
	 * Reset all errors if valid
	 */
	public function resetErrors(){

		if($this->alertHandler !== null){
			//dump($this->alertHandler,$this->alertHandler->getReset($this->getId()));
			echo $this->alertHandler->getReset($this->getId());
			//return $this->alertHandler->getReset($this->getId());
		}
	}

	/**
	 * Render error message
	 * @param array $req
	 */
	public function renderError(array $req){
		if ( ! empty( $req ) ) {
			if($this->alertHandler === null ) {
				echo '<' . $this->alertContainerElementStart . '>';
				foreach ( $req as $r ) {
					echo '<' . $this->alertElementStart . '>';
					echo $r;
					echo '</' . $this->alertElementEnd . '>';
				}
				echo '</' . $this->alertContainerElementEnd . '>';
			} else {
				echo $this->alertHandler->handle($req,$this->getId());
			}
		} else {
			echo '<div class="alert alert-danger">Rendering errors, but no errors</div>';
		}
	}

	/**
	 * Render from in call object as string
	 * @return string
	 */
	public function __toString() {
		try {
			$output = $this->draw();
		} catch(Exception $e) {}
		return $output;
	}

	/**
	 * Hide Form with javascript
	 */
	public function hide(){
		echo '<script>$("#'.$this->getFormID().'").hide()</script>';
	}

	/**
	 * Redirect with delay in javascript
	 * @param string $href
	 * @param null $sec
	 * @param null $js
	 */
	public function redirect($href, $sec=null, $js = null){
		$timeout = (int) $sec * 1000;
		if ( $sec === null ) {
			$timeout = 2000;
		}
		$url = $href;
		echo '<script>setTimeout(function(){' . $js . ';location.href="'.$url.'";},' . $timeout . ');</script>';
	}
	public function refresh($sec = null,$js = null){
		$timeout = (int) $sec * 1000;
		if ( $sec === null ) {
			$timeout = 2000;
		}
		$url = $_SERVER["REQUEST_URI"];
		echo '<script>setTimeout(function(){' . $js . ';location.href="'.$url.'";},' . $timeout . ');</script>';

	}

	public function sendResponse(IResponse $render){
		$render->setForm($this);
		echo $render->render();
	}

	#[\ReturnTypeWillChange] public function offsetUnset( $offset ) {
		// TODO: Implement offsetUnset() method.
	}
	#[\ReturnTypeWillChange] public function offsetSet( $offset, $value ) {
		// TODO: Implement offsetSet() method.
	}

	/**
	 * @param mixed $offset
	 *
	 * @return FormItem
	 */
	#[\ReturnTypeWillChange] public function offsetGet( $offset ) {
		return $this->getFormItemByName($offset);
	}
	#[\ReturnTypeWillChange] public function offsetExists( $offset ) {
		return ($this->getFormItemByName($offset) !== null) ? true : false;
		// TODO: Implement offsetExists() method.
	}

	/**
	 * @param bool $noescapeMode
	 *
	 * @return Form
	 */
	public function setNoescapeMode( bool $noescapeMode = true ): Form {
		$this->noescapeMode = $noescapeMode;

		return $this;
	}

}

class FormException extends \Exception {
	/** @var null  */
	private $domId;

	/**
	 * FormException constructor.
	 *
	 * @param string $message
	 * @param null $domId
	 * @param null $code
	 * @param Exception|null $previous
	 */
	public function __construct( $message, $domId = null, $code = 500, Exception $previous = null ) {
		parent::__construct( $message, $code, $previous );
		$this->domId = $domId;
	}

	/**
	 * @return string
	 */
	public function getDomId(){
		return $this->domId;
	}
}