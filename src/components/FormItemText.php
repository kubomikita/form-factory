<?php
namespace Kubomikita;
use Nette\Utils\Strings;

/**
 * Textovy input
 */
class FormItemText extends FormItem {
	private $size;
	protected $type = "text";
	protected $progressCreated;

	protected $siblings = [];
	protected $multilang = false;
	protected $defaultLanguage = "sk";

	/**
	 * @param array $siblings
	 */
	public function setSiblings( array $siblings ) {
		$this->siblings = $siblings;
		return $this;
	}

	public function setMultilang(bool $bool = true){
		$this->multilang = $bool;
		return $this;
	}

	/**
	 * @param string $defaultLanguage
	 */
	public function setDefaultLanguage( string $defaultLanguage ) {
		$this->defaultLanguage = $defaultLanguage;
	}



	function setSize($size) {
		$this->size = $size;
		return $this;
	}

	function setType($type) {
		$this->type = $type;
		return $this;
	}

	function getType() {
		return $this->type;
	}

	/**
	 * @param $field
	 * @param $text
	 *
	 * @return $this
	 */
	function setEqualTo($field,$text = null){
		$this->equalTo = $field;
		$this->equalToText = $text;
		$ar = array("field"=>$this->equalTo,"text"=>$this->equalToText);
		$this->form->setSession("FormDependsOn",$ar);
		return $this;
	}

	function getProgressBar(){
		$ret = "";
		//$this->main
		if($this->form->getAjax() and $this->type=="file" and !$this->progressCreated){
			$this->progressCreated = true;
			$ret .= '<div class="progress" id="progress_'.$this->form->getId().'">
                        <div class="bar"></div >
                        <div class="percent">0%</div >
                    </div>';
		}
		return $ret;
	}

	public function getHTML($itemStart=null,$itemEnd=null) {
		$ret=$this->getLabel();
		if($this->getItemStart($itemStart) !== null) {$ret.='<'.$this->getItemStart($itemStart).'>';}
		if($this->prepend!==null){
			$ret .= '<div class="input-group-prepend" id="'.$this->getHtmlId().'-prepend"><div class="input-group-text">'.$this->prepend.'</div></div>';
		}
		$ret .= $this->getControl([],$itemStart,$itemEnd);
		$ret .= $this->getAppend();
		if($this->getItemEnd($itemEnd) !== null) {$ret.='</'.$this->getItemEnd($itemEnd).'>';}
		return $ret;
	}
	public function addPrepend($string){
		$this->prepend = $string;
		return $this;
	}
	public function addAppend($string,$btn = false){
		$this->append = $string;
		$this->appendBtn = $btn;
		return $this;
	}
	public function getLabel($attrs=[]) {
		if($this->multilang){
			return;
		}
		return '<label for="'.$this->getHtmlId().'" '.$this->processLabelAttr($attrs).'>'.$this->label.'</label>';
	}

	protected function renderControl($attrs = []){
		return '<input type="'.$this->type.'" id="'.$this->getHtmlId().'" name="'.$this->name.'" value="'.$this->getItemValue().'" '.$this->processAttr($attrs).'>';
	}

	public function getAppend(){
		$ret = "";
		if($this->append!==null){
			$ret .= '<div class="input-group-append" id="'.$this->getHtmlId().'-append">';
			if(!$this->appendBtn) {
				$ret .= '<div class="input-group-text">' . $this->append . '</div>';
			} else {
				$ret .= $this->append;
			}
			$ret .= '</div>';
		}
		return $ret;
	}


}