<?php


namespace Kubomikita;


class FormItemSubmit extends FormItem {

	public function __construct($name,$label,$form) {
		$this->form=$form;
		$this->render=false;
		$this->label = $this->form->getFormFactory()->translate($label);
		$this->setName($name);
		$this->type = "submit";

		$this->setAttr("class", "btn btn-success");
		$this->setDivAttr("class","form-actions text-right");
	}

	public function getHTML($itemStart=null,$itemEnd=null) {
		$ret = "";
		if($this->getItemStart($itemStart) !== null) {$ret.='<'.$this->getItemStart($itemStart).'>';}
		$ret .= $this->getControl();
		if($this->getItemEnd($itemEnd) !== null) {$ret.='</'.$this->getItemEnd($itemEnd).'>';}
		return $ret;
	}
	public function getLabel($attrs=[]) {
		return "";
	}
	public function getControl($attrs=[]) {
		return '<button type="submit" name="form_id" value="'.$this->form->getId().'" '.$this->processAttr($attrs).'>'.$this->label.'</button>';
	}
}