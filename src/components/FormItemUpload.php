<?php
namespace Kubomikita;
/**
 * Upload input
 */
class FormItemUpload extends FormItem {
	private $size;
	protected $type = "file";
	protected $progressCreated;

	protected $multiupload = false;

	/**
	 * @param bool $multiupload
	 *
	 * @return $this
	 */
	public function setMultiupload( $multiupload = true ) {
		$this->multiupload = $multiupload;
		return $this;
	}

	function getProgressBar(){
		$ret = "";
		//$this->main
		if($this->form->getAjax() and $this->type=="file" and !$this->progressCreated){
			$this->progressCreated = true;
			$ret .= '<div class="progress" id="progress_'.$this->form->getId().'">
                        <div class="bar"></div >
                        <div class="percent">0%</div >
                    </div>';
		}
		return $ret;
	}

	public function getHTML($itemStart=null,$itemEnd=null) {
		//dump($this->getItemStart($itemStart),$itemEnd);
		//var_dump();
		$ret=$this->getLabel();//'<label for="'.$this->form->getId().'-'.$this->name.'" class="'.$this->labelClass.'">'.$this->label.'</label>';
		/*if($this->multilang){
			$ret.='<div '.$this->getDivAttrs().'>'.FormBlock::generateInput($this). $this->getDesc(). '</div>';
		} else {*/
		if($this->getItemStart($itemStart) !== null) {$ret.='<'.$this->getItemStart($itemStart).'>';}
		if($this->prepend!==null){
			$ret .= '<div class="input-group-prepend" id="'.$this->form->getId().'-'.$this->name.'-prepend"><div class="input-group-text">'.$this->prepend.'</div></div>';
		}
		$ret.=$this->getControl();
		if($this->append!==null){
			$ret .= '<div class="input-group-append" id="'.$this->form->getId().'-'.$this->name.'-append">';
			if(!$this->appendBtn) {
				$ret .= '<div class="input-group-text">' . $this->append . '</div>';
			} else {
				$ret .= $this->append;
			}
			$ret .= '</div>';
		}
		if($this->getItemEnd($itemEnd) !== null) {$ret.='</'.$this->getItemEnd($itemEnd).'>';}
		return $ret;
	}
	public function addPrepend($string){
		$this->prepend = $string;
		return $this;
	}
	public function addAppend($string,$btn = false){
		$this->append = $string;
		$this->appendBtn = $btn;
		return $this;
	}
	public function getLabel($attrs = []) {
		return '<label for="'.$this->form->getId().'-'.$this->name.'" class="'.$this->labelClass.'">'.$this->label.'</label>';
	}
	public function getControl($attrs=[]) {
		$name = $this->getName();
		if($this->multiupload){
			$name .= "[]";
			$attrs["multiple"] = "multiple";
		}

		return '<input type="'.$this->type.'" id="'.$this->form->getId().'-'.$this->name.'" name="'.$name.'" value="'.$this->getItemValue().'" '.$this->processAttr($attrs).' />';
	}


}