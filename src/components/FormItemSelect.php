<?php


namespace Kubomikita;


class FormItemSelect extends FormItem {

	// Zoznam poloziek
	private $items = array();
	private $selected = array();
	protected $type = "select";
	protected $multiple = false;
	protected $dataAttrs = [];

	public function setDataAttrs(array $attrs) : self {
		$this->dataAttrs = $attrs;
		return $this;
	}

	public function __construct($name, $label,$form, $items) {
		parent::__construct($name, $label,$form);
		$this->items = $items;
		if($this->translateItems) {
			foreach($items as $k => $v){
				if(! ($v instanceof \LangStr)) {
					if(!is_array($v)) {
						$this->items[ $k ] = $this->form->getTranslator()->translate( $v );
					} else {
						$vals = [];
						foreach($v as $k1 => $v1){
							$vals[$k1] = $this->form->getTranslator()->translate($v1);
						}
						$this->items[ $k ] = $vals;
					}
				}
			}
		}
	}
	public function setSelected($val){
		if(!is_array($val)) $val=array($val);
		$this->selected=$val;
		return $this;
	}
	public function getHTML($itemStart=null,$itemEnd=null) {
		$ret = $this->getLabel();
		if($this->getItemStart($itemStart) !== null) {$ret.='<'.$this->getItemStart($itemStart).'>';}
		$ret.=$this->getControl();
		if($this->getItemEnd($itemEnd) !== null) {$ret.='</'.$this->getItemEnd($itemEnd).'>';}
		return $ret;
	}
	public function getLabel($attrs=[]) {
		return '<label for="'.$this->form->getId().'-'.$this->name.'" '.$this->processLabelAttr($attrs).'>'.$this->label.'</label>';
	}
	public function getAppend() {
		return '';
	}

	public function getControl($attrs=[]) {
		$name = $this->name;
		if($this->multiple){
			$name .= "[]";
			$attrs["multiple"] = "multiple";
		}

		$ret = '<select id="'.$this->form->getId().'-'.$this->name.'" name="'.$name.'"  '.$this->processAttr($attrs).'>'; //value="'.$this->getItemValue().'"
		foreach ($this->items as $item => $key) {
			if(is_array($key)){
				//var_dump($item,$key);
				$ret .= "<optgroup label=\"$item\">";
				foreach($key as $k=>$v){
					$s=(in_array($k, $this->selected))?"selected":"";
					$ret .=  '<option value="'.$k.'" '.$s.'>'.$v.'</option>';
				}
				$ret .= "</optgroup>";
			} else {
				$s=(in_array($item, $this->selected))?"selected":"";
				$ret .=  '<option value="'.$item.'" '.$s.' '.$this->processDataAttr($item).'>'.$key.'</option>';
			}
		}
		$ret .= '</select>';
		return $ret;

	}

	public function processDataAttr(string $key){
		if(isset($this->dataAttrs[$key])){
			$attrs = [];
			foreach ($this->dataAttrs[$key] as $k => $v){
				$attrs[] = $k.'="'.$v.'"';
			}
			return implode(" ", $attrs);
		}
		return '';
	}

	/**
	 * @param bool $multiple
	 *
	 * @return FormItemSelect
	 */
	public function setMultiple( bool $multiple = true ): FormItemSelect {
		$this->multiple = $multiple;
		return $this;
	}

	/**
	 * @param array $items
	 *
	 * @return FormItemSelect
	 */
	public function setItems( array $items ): FormItemSelect {
		$this->items = $items;

		return $this;
	}
}
