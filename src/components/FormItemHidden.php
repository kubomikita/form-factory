<?php


namespace Kubomikita;


use Latte\Engine;

class FormItemHidden extends FormItem {

	public function __construct($name, $value,$form) {
		parent::__construct($name, null,$form);
		$this->value = $value;
		$this->type = "hidden";
		$this->render=false;
	}
	public function getHTML($itemStart=null,$itemEnd=null) {
		return $this->getControl();
	}
	public function getLabel($attrs=[]) {
		return "";
	}
	public function getControl( $attrs = [ ] ) {
		/** @var Engine $latte */
		$latte = $this->form->context->getService("latte");
		return $latte->renderToString(__DIR__."/templates/ItemHidden.latte",["id" => $this->form->getId().'-'.$this->name, "name"=> $this->name, "value" => $this->value]);
	}
}
