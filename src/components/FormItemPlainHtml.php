<?php


namespace Kubomikita;

/**
 * @deprecated
 */
class FormItemPlainHtml extends FormItem {
	protected $html,$class = "plain_html";
	public function __construct($html,$form){
		$this->html = $html;
		$this->form = $form;
		$this->render=false;
	}
	/*public function setClass($c){
		$this->class = $c;
		return $this;
	}*/
	public function getHTML($itemStart=null,$itemEnd=null){
		$ret = '<div '.$this->getAttrs().' >'.$this->html.'</div>';
		return $ret;
	}
	public function getLabel() {
		return '';
	}
	public function getControl($attrs=[]) {
		return $this->html;
	}
}