<?php
namespace Kubomikita;
/**
 * Textovy input
 */
class FormItemCheckbox extends FormItem {
	private $size;
	protected $type = "checkbox";
	protected $list = [];

	public function __construct($name, $label,array $list , $form ) {
		parent::__construct( $name, $label, $form );
		$this->setAttr("class","form-check-input");
		$this->setValue(1);
		$this->list = $list;
		//          $this->label
	}


	function getType() {
		return $this->type;
	}

	public function setChecked(){
		$this->attrs["checked"] = "checked";
	}

	/**
	 * @param $field
	 * @param $text
	 *
	 * @return $this
	 */
	function setEqualTo($field,$text){
		$this->equalTo = $field;
		$this->equalToText = $text;
		$ar = array("field"=>$this->equalTo,"text"=>$this->equalToText);

		$this->form->setSession("FormDependsOn",$ar);

		return $this;
	}

	public function getHTML($itemStart=null,$itemEnd=null) {

		if(!empty($this->list)) {
			$ret = $this->getControl();
		} else {
			$ret = '<div class="form-check">';
			$ret .= $this->getControl();
			$ret .= $this->getLabel();
			$ret .= '</div>';
		}
		//var_dump();
		/*$ret='<label for="'.$this->form->getId().'-'.$this->name.'" class="'.$this->labelClass.'">'.$this->label.'</label>';

			$value = (isset($_POST[$this->name]) and $_POST["form_id"] == $this->getFormID())?$_POST[$this->name]:$this->value;
			$ret.='<div '.$this->getDivAttrs().'>
                        <input type="'.$this->type.'" id="'.$this->form->getId().'-'.$this->name.'" name="'.$this->name.'" value="'.$value.'" '.$this->getAttrs().' />'.$this->getDesc().$this->getProgressBar().'
                    </div>';
		*/

		return $ret;
	}
	public function getLabel($attrs=[]) {
		return '<label for="'.$this->form->getId().'-'.$this->name.'" '.$this->processLabelAttr($attrs).'>'.$this->label.'</label>';
	}
	public function getControl($attrs=[]) {

		if(!empty($this->list)){
			$ret= [];
			$i = 0;
			foreach($this->list as $k=>$v){
				$ret[] = '<input type="'.$this->type.'" id="'.$this->form->getId().'-'.$this->name."-".$i.'" name="'.$this->name.'" value="'.$k.'" '.$this->processAttr($attrs).' />';
				$i++;
			}
			return implode(" ",$ret);
			//return "";
		}
		return '<input type="'.$this->type.'" id="'.$this->form->getId().'-'.$this->name.'" name="'.$this->name.'" value="'.$this->getItemValue().'" '.$this->processAttr($attrs).' />';
	}
}