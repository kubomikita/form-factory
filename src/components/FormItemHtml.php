<?php
namespace Kubomikita;

class FormItemHtml extends FormItem {
	protected $html;
	public function __construct($name,$label,$html,$form){
		parent::__construct($name, $label, $form);
		$this->html=$html;
	}
	public function getHTML(){
		//var_dump($this->labelClass);
		//$this->addDivAttr("class","simply-text");
		$ret='<label class="'.$this->labelClass.'">'.$this->label.'</label>';
		$ret.='<div '.$this->getDivAttrs().'>'.$this->html.$this->getDesc().'</div>';
		return $ret;
	}
}