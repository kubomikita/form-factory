<?php


namespace Kubomikita;


use Nette\InvalidArgumentException;
use Tracy\Debugger;

class FormItemRadio extends FormItem {
	public $items = array();
	private $checked = false;



	public function __construct($name, $label,$form, $items) {
		parent::__construct($name, $label,$form);
		if(is_array($this->label)){
			$this->labelClass = $this->label[1];
			$this->label = $this->label[0];
		} else {
			if(is_array($label)){
				$this->setLabel($label[0]);
				$this->labelClass=$label[1];
			}
		}
		$items = ($items !== null ? $items : []);
		$this->items = $items;
		if($this->translateItems) {
			foreach($items as $k => $v){
				if(! ($v instanceof \LangStr)) {
					$this->items[ $k ] = $this->form->getTranslator()->translate( $v );
				}
			}
		}
		$this->type = "radio";
	}

	public function setChecked($val){
		$this->checked = (is_numeric($val) ? (int) $val : $val);
		return $this;
	}
	public function setItems( array $items ): FormItemRadio {
		$this->items = $items;

		return $this;
	}

	public function getHTML($itemStart=null,$itemEnd=null) {
		//dump($this->itemStart,$itemStart,$this->processAttr(),$this->getAttrs());
		//$ret = '<label class="'.$this->labelClass.'">'.$this->label.'</label> <div '.$this->getDivAttrs().">";
		$ret = $this->getLabel();
		$i = 0; $items = [];
		foreach ($this->items as $item => $key) {
			$ch=($item===$this->checked)?"checked":"";

			//dump($this->checked,$item,$key,$ch);
			if($this->getItemStart($itemStart) !== null) {$ret.='<'.$this->getItemStart($itemStart).'>';}
			$ret .= '<input type="radio" value="'.$item.'" id="'.$this->form->getId().'-'.$this->name.'_'.$i.'" name="'.$this->name.'" '.$this->getAttrs().' '.$ch.'><label class="form-check-label" for="'.$this->form->getId().'-'.$this->name.'_'.$i.'">'.$key.'</label>                ';
			if($this->getItemEnd($itemEnd) !== null) {$ret.='</'.$this->getItemEnd($itemEnd).'>';}
			$i++;
		}


		return $ret;
	}
	public function getLabel($attrs=[]) {
		return '<label for="'.$this->form->getId().'-'.$this->name.'" '.$this->processLabelAttr($attrs).'>'.$this->label.'</label>';
	}
	public function getControl($attrs=[],$part = null,$type="inputs") {
		$ret = $inputs = $labels = [];
		$i = 0;
		foreach ($this->items as $item => $key) {
			if($item === $this->checked){
				$attrs["checked"] = "checked";
			} else {
				unset($attrs["checked"]);
			}
			//dump($attrs,$item,$this->checked);

			$input = '<input type="radio" value="'.$item.'" id="'.$this->form->getId().'-'.$this->name.'-'.$item.'" name="'.$this->name.'" '.$this->processAttr($attrs).'>';
			$label = '<label for="'.$this->form->getId().'-'.$this->name.'-'.$item.'" '.$this->processLabelAttr($attrs).'>'.$key.'</label>';

			$inputs[$item] = $input;
			$labels[$item] = $label;
			if(($tagStart = $this->getItemStart(null)) !== null) { $ret[$item][]='<'.$tagStart.'>'; }
			$ret[$item][] = $input.$label;
			if(($tagEnd = $this->getItemEnd(null)) !== null) {$ret[$item][] = '</'.$tagEnd.'>';}

			$ret[$item] = implode("", $ret[$item]);
			$i++;
		}

		if($part !== null){
			return ${$type}[$part];
		}
		//return '<input type="'.$this->type.'" id="'.$this->form->getId().'-'.$this->name.'" name="'.$this->name.'" value="'.$this->getItemValue().'" '.$this->processAttr($attrs).' />';

		return implode(" ",$ret);
	}
	public function getControlPart($part,$attrs=[]){
		$control = $this->getControl($attrs,$part);
		if($control === null){
			throw new InvalidArgumentException("Part of control <i>'".$this->getName()."'</i> of type <i>'".$this->getType()."'</i>  no exists - part num: <i>'".$part."'</i> ");
		}
		return $control;
	}
	public function getLabelPart($part,$attrs=[]){
		$labels = $this->getControl($attrs,$part,"labels");
		$this->render=false;
		return $labels;
	}
}

