<?php
namespace Kubomikita;

use MatthiasMullie\Minify;
use Nette\DI\Container;
use Nette\Http\Request;

/**
 * Tovarnicka na vykreslenie formularov. Samotny formular sa sklada z blokov,
 * kde su jednotlive polia. Tato trieda zaobstarava vykreslenie celeho formulara.
 *
 * @author Jakub Mikita <kubomikita@gmail.com>
 * @version 2.0
 */
class FormFactory {

	const GENERATOR = "commerce/form-factory";

	public static $registered = false;
	private static $js_file = null;
	private static $with_jquery = true;

	protected $requestAjax;


	/**
	 * @var IFromAjaxHandler
	 */
	private $ajax_handler = null;
	/** @var IFormAlertHandler  */
	private $alert_handler = null;
	/** @var IFormTranslator */
	private $translator = null;

	private $defaultRequiredText = "%s je povinné.";
	private $defaultEqualText = "Pole '%s' nieje rovnaké ako pole '%s'.";
	private $multilang = [];

	public function __construct(Request $request = null) {
		if($request !== null) {
			$this->requestAjax = $request->isAjax();
		}
	}

	public function register(){
		if(!(session_status() == PHP_SESSION_ACTIVE)){
			throw new \Exception("Unable to start FormFactory. Sessions not started. Please start session.");
		}
		if($this->translator === null){
			$this->setTranslator(new NullFormTranslator());
		}

		$_SESSION[static::GENERATOR]["form-factory"] = $this;
		static::$registered = true;
		if($this->requestAjax === null) {
			//bdump($this->request->isAjax());
			if ( isset( $_SERVER["HTTP_X_REQUESTED_WITH"] ) && $_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest" ) {
				if ( $this->ajax_handler === null ) {
					throw new \Exception( "Ajax handler not defined" );
				}
				$this->ajax_handler->handle();
			}
		} else {
			if($this->requestAjax){
				if ( $this->ajax_handler === null ) {
					throw new \Exception( "Ajax handler not defined" );
				}
				$this->ajax_handler->handle();
			}
		}
	}

	public function getDefaultRequiredText(){
		return $this->translate($this->defaultRequiredText);
	}
	public function getDefaultEqualText(){
		return $this->translate($this->defaultEqualText);
	}
	public function getAjaxHandler(){
		return $this->ajax_handler;
	}
	public function getAlertHandler(){
		return $this->alert_handler;
	}

	/**
	 * @return array
	 */
	public function getMultilang(): array {
		return $this->multilang;
	}

	/**
	 * @return IFormTranslator
	 */
	public function getTranslator(){
		return $this->translator;
	}
	public function setAjaxHandler(IFromAjaxHandler $handler){
		$this->ajax_handler = $handler;
	}
	public function setAlertHandler(IFormAlertHandler $handler){
		$this->alert_handler = $handler;
	}
	public function setJavascriptFilename($name){
		static::$js_file = $name;
	}
	public function setWithJquery($bool) {
		static::$with_jquery = (bool) $bool;
	}
	public function setTranslator(IFormTranslator $translator){
		$this->translator = $translator;
	}

	/**
	 * @param array $multilang
	 */
	public function setMultilang( array $multilang ) {
		$this->multilang = $multilang;
	}

	public function translate(string $string): string {
		if($this->translator instanceof IFormTranslator){
			return $this->translator->translate($string);
		}
		return $string;
	}

	public static function function_exists($function){
		if($function === null){
			return false;
		}
		if(strpos($function, "::") !== false){
			list($c,$m)=explode("::",$function);
			return method_exists($c, $m);
		}
		return function_exists($function);
	}
	public static function scripts(){
		if(static::$js_file === null) {
			$jsmin = new Minify\JS( __DIR__ . "/formfactory.ajax.js" );
			$js    = "<script type=\"text/javascript\">{$jsmin->minify()}</script>";
		} else {
			$checksum = null;
			if(file_exists(static::$js_file)){
				$checksum = md5_file(static::$js_file);
			}
			$jsmin = new Minify\JS( __DIR__ . "/formfactory.ajax.js" );
			$min = $jsmin->minify();//static::$js_file
			if($checksum !== md5($min)){
				file_put_contents(static::$js_file,$min);
			}
			$js = '<script type="text/javascript" src="'.static::$js_file.'"></script>';
		}
		if(static::$registered) {
			if(static::$with_jquery) {
				echo <<<EOT
		<script type="text/javascript">window.jQuery || document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"><\/script>');</script>
		$js
EOT;
			} else {
				echo $js;
			}
		}

	}
	public static function js(){
		if(static::$js_file === null) {
			$jsmin = new Minify\JS( __DIR__ . "/formfactory.ajax.js" );
			$js    = $jsmin->minify();
		} else {
			$checksum = null;
			if(file_exists(static::$js_file)){
				$checksum = md5_file(static::$js_file);
			}
			$jsmin = new Minify\JS( __DIR__ . "/formfactory.ajax.js" );
			$min = $jsmin->minify();//static::$js_file
			if($checksum !== md5($min)){
				file_put_contents(static::$js_file,$min);
			}
			$js = static::$js_file;
		}
		return $js;
	}
	public static function quote($string, $noescapeMode = false){
		if($noescapeMode){
			return $string;
		}
		if(is_string($string)) {
			return htmlspecialchars( $string );
		} /*elseif (is_array($string)){
			return array_map("htmlspecialchars", $string);
		}*/
		return $string;
	}

}
