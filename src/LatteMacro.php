<?php
namespace Kubomikita\Latte\Macro;

use Kubomikita\Form;
use Kubomikita\FormItem;
use Latte\Compiler;
use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;
use Nette\Utils\Strings;
use Exception;

class FormFactoryMacro extends MacroSet{

	public static function install(Compiler $compiler){
		$me = new static($compiler);
		$me->addMacro("form", [$me, "macroFormStart"],[$me, "macroFormEnd"]);
		$me->addMacro("input", [$me, "macroInput"]);
		$me->addMacro("label", [$me, "macroLabel"]);
		$me->addMacro("inputError", [$me, "macroInputError"]);
		$me->addMacro("controlPart", [$me, "macroControlPart"]);
		$me->addMacro("labelPart",[$me,"macroLabelPart"]);
		$me->addMacro("control", [$me, "macroControl"]);
	}

	public function macroControl(MacroNode $node, PhpWriter $writer) {
		if ($node->modifiers) {
			throw new CompileException('Modifiers are not allowed in ' . $node->getNotation());
		}
		$words = $node->tokenizer->fetchWords();
		if (!$words) {
			throw new CompileException('Missing name in ' . $node->getNotation());
		}

		$name = array_shift($words);
		return $writer->write('echo $controller[\''.$name.'\'];');

	}

	public function macroFormStart(MacroNode $node, PhpWriter $writer){
		if ($node->modifiers) {
			throw new CompileException('Modifiers are not allowed in ' . $node->getNotation());
		}
		$words = $node->tokenizer->fetchWords();
		if (!$words) {
			throw new CompileException('Missing name in ' . $node->getNotation());
		}

		$name = array_shift($words);
		$varname = $this->getFormVar($node);
		$w = $writer->write('if(isset($controller) && $controller instanceof CommerceController && ($form = $controller->getComponent(\''.$name.'\', true)) !== false){ '.$varname.' = $form; } else { '.$varname.' = $form = Kubomikita\\Form::getInstance("'.$name.'"); }'."\n");
		$w .= $writer->write('echo '.$varname.'->start();');
		return $w;
	}

	public function macroInputError(MacroNode $node, PhpWriter $writer){
		
		if($node->args != ""){
			$name = $node->args;
			$varname = $this->getFormVar($node);
			$w = $writer->write('if(isset($controller) && $controller instanceof CommerceController && ($inputErrorForm = $controller->getComponent(\''.$name.'\', true)) !== false){ '.$varname.' = $inputErrorForm; unset($inputErrorForm); } else { '.$varname.'  = Kubomikita\\Form::getInstance("'.$name.'"); }'."\n");
			//$w .= $writer->write('dump('.$varname.')');
			$w .= $writer->write('echo '.$varname.'->errors();');
			//dumpe($name, $varname, $w);
			/*$form = Form::getInstance($instance);
			if($form === null){
				throw new Exception("Form with id '{$node->args}' not exists.".($instance == ""?" Check if is errors container inside from.":""));
			}*/
			 //$writer->write("echo \Kubomikita\Form::getInstance('".$instance."')->errors();");

		} else {
			$instance = $node->parentNode->args;

			$varname = $this->getFormVar($node);
			//dumpe($instance, $varname,$node);
			$w = $writer->write('echo '.$varname.'->errors();');
		}
		return $w;
	}

	public function macroFormEnd(MacroNode $node, PhpWriter $writer){
		if ($node->modifiers) {
			throw new CompileException('Modifiers are not allowed in ' . $node->getNotation());
        }
		$words = $node->tokenizer->fetchWords();
        if (!$words) {
		    throw new CompileException('Missing name in ' . $node->getNotation());
        }

        $name = array_shift($words);
		$varname = $this->getFormVar($node);
		$w = $writer->write('echo '.$varname.'->end();');
		//$w .= $writer->write("dump(".$varname.");");
		return $w;
	}
	public function macroInput(MacroNode $node, PhpWriter $writer){
		if ($node->modifiers) {
			throw new CompileException('Modifiers are not allowed in ' . $node->getNotation());
		}
		$words = $node->tokenizer->fetchWords();
		if (!$words) {
			throw new CompileException('Missing name in ' . $node->getNotation());
		}
		$name = array_shift($words);
		$varname = $this->getFormVar($node);
		//$w = '';
		$w = $writer->write('echo '.$varname.'->getControl("'.$name.'", %node.array)');
		return $w;
	}
	public function macroControlPart(MacroNode $node, PhpWriter $writer){
		if ($node->modifiers) {
			throw new CompileException('Modifiers are not allowed in ' . $node->getNotation());
		}
		$words = $node->tokenizer->fetchWords();
		if (!$words) {
			throw new CompileException('Missing name in ' . $node->getNotation());
		}
		$name = array_shift($words);
		$args = Strings::split($node->args,'~,\s*~');
		unset($args[0]);
		$controlId = $args[1];
		unset($args[1]);
		$attrs = implode("|",$args);

		$varname = $this->getFormVar($node);
		$w = $writer->write('echo '.$varname.'->getControlPart("'.$name.'","'.$controlId.'", %node.array)');
		return $w;
	}
	public function macroLabelPart(MacroNode $node, PhpWriter $writer){
		if ($node->modifiers) {
			throw new CompileException('Modifiers are not allowed in ' . $node->getNotation());
		}
		$words = $node->tokenizer->fetchWords();
		if (!$words) {
			throw new CompileException('Missing name in ' . $node->getNotation());
		}
		$name = array_shift($words);
		$args = Strings::split($node->args,'~,\s*~');
		unset($args[0]);
		$controlId = $args[1];
		unset($args[1]);
		$attrs = implode("|",$args);
		$varname = $this->getFormVar($node);
		$w = $writer->write('echo '.$varname.'->getLabelPart("'.$name.'","'.$controlId.'", %node.array)');
		return $w;
	}
	public function macroLabel(MacroNode $node, PhpWriter $writer){
		if ($node->modifiers) {
			throw new CompileException('Modifiers are not allowed in ' . $node->getNotation());
		}
		$words = $node->tokenizer->fetchWords();
		if (!$words) {
			throw new CompileException('Missing name in ' . $node->getNotation());
		}
		$name = array_shift($words);
		$varname = $this->getFormVar($node);
		$w = $writer->write('echo '.$varname.'->getLabel("'.$name.'", %node.array)');
		return $w;
	}


	private function getFormId(MacroNode $node){
		if($node->name == "form" || ($node->name == "inputError" && $node->args !== "")){
			$parent = $node;
		} else {
			$parent = $node->parentNode;
			if ( $parent->name != "form" ) {
				$parent = $parent->parentNode;
			}
		}
		return $parent->args;
	}
	private function getFormVar(MacroNode $node){
		return '$Forms[\''.($this->getFormId($node)).'\']';
	}
}