<?php
namespace Kubomikita;
use Nette\Application\AbortException;
use Nette\DeprecatedException;
use Nette\InvalidArgumentException;
use Nette\InvalidStateException;

/**
 * Trieda reprezentujca blok vo formulary. Do bloku su vkladane polozky formulara
 * a nasledne je potrebne tento blok vlozit do formulara.
 *
 * @author Jakub Mikita <kubomikita@gmail.com>
 * @version 1.0
 */
class FormBlock {

    // pole poloziek vo formulary
    private $items = array();
    // pocitadlo poloziek
    private $counter = 0;
    private $blockAttrs = array();
    private $beforeElement,
	        $afterElement,
	        $labelClass,
	        $itemClass,
	        $blockStart,
	        $blockEnd,
	        $itemStart,
	        $itemEnd;
	/** @var Form  */
    private $id;
    private $title=null;
	private $titleStart="h1",
			$titleEnd= "h1";
    private $wizard=false;
    //private $render=true;

	/** @deprecated  */
	public function setElementParent(){
		return;
	}
	/** @deprecated  */
	public function setElementClass(){
		return;
	}
	/** @deprecated  */
	public function setAttr(){
		return;
	}
    public function __construct($formID) {
	    $this->id = $formID;
    }

	/**
	 * Get all items of form block
	 * @return array
	 */
    public function getItems(){
        return $this->items;
    }

    public function getComponent($name) : FormItem {
    	/** @var FormItem $item */
	    foreach($this->items as $item){
	    	if($item->getName() === $name){
	    		return $item;
		    }

		}
	    throw new InvalidArgumentException("Component with name '$name' not exists!");
    }

	/**
	 * Checks if FormItem already exists
	 * @param $name
	 *
	 * @throws AbortException
	 */
    private function isExists($name){
    	/** @var FormItem $item */
	    foreach ($this->getItems() as $item){
    		if($name == $item->getName()){
				throw new AbortException("FormItem with name '".$name."' already exists!");
		    }
	    }
    }

    public function addUpload($name, $label) : FormItemUpload {
    	$this->isExists($name);
	    $this->items[$this->counter] = new FormItemUpload($name, $this->getLabelArray($label), $this->id);
	    $this->defaultItemClass($this->items[$this->counter]);
	    return $this->items[$this->counter++];
    }

	/**
	 * @param $name
	 * @param $label
	 *
	 * @return FormItemText
	 */
    public function addText($name, $label) : FormItemText {
    	$this->isExists($name);
        $this->items[$this->counter] = new FormItemText($name, $this->getLabelArray($label), $this->id);
		if($this->id->isStrict()) {
			if(version_compare(PHP_VERSION, '7.3.0') > 0) {
				$pattern = Form::PATTERN_ALPHANUMERIC_73;
			} else {
				$pattern = Form::PATTERN_ALPHANUMERIC;
			}
			$this->items[ $this->counter ]->setPattern( "Zadávajte iba alfanumerické znaky <strong>bez znakov _$!?|[]()*'\"+/</strong>",
				 $pattern);
		}
        $this->defaultItemClass($this->items[$this->counter]);
        return $this->items[$this->counter++];
    }

    public function addLangText($name, $label, string $defaultLanguage = "sk") : FormItemText {
    	if($this->id instanceof Form && empty($this->id->getMultilang())){
    		throw new InvalidStateException("Form is no multilang. Please set multilang option.");
	    }
	    $languages = $this->id->getMultilang();
		//bdump($name);
    	[, $name ] = explode("-",$name);
    	$stack = [];
		//bdump($name);
    	foreach($languages as $lg){
    		$formatName = "txt-".$lg."-".$name;
    		$this->items[$this->counter] = new FormItemText($formatName, $this->getLabelArray($label), $this->id);
    		$this->defaultItemClass($this->items[$this->counter]);
    		if($lg == $defaultLanguage){
    			$defaultInput = $this->items[$this->counter];
		    } else {
			    $stack[] = $this->items[ $this->counter ];
			    $this->items[ $this->counter ]->setMultilang();
		    }
		    $this->items[ $this->counter ]->setDefaultLanguage($defaultLanguage);
    		$this->counter++;
	    }

    	$defaultInput->setSiblings($stack);

    	return $defaultInput;
    }
	public function addLangTextarea($name, $label, string $defaultLanguage = "sk") : FormItemTextarea {
		if($this->id instanceof Form && empty($this->id->getMultilang())){
			throw new InvalidStateException("Form is no multilang. Please set multilang option.");
		}
		$languages = $this->id->getMultilang();

		[, $name ] = explode("-",$name);
		$stack = [];

		foreach($languages as $lg){
			$formatName = "txt-".$lg."-".$name;
			$this->items[$this->counter] = new FormItemTextarea($formatName, $this->getLabelArray($label), $this->id);
			$this->defaultItemClass($this->items[$this->counter]);
			if($lg == $defaultLanguage){
				$defaultInput = $this->items[$this->counter];
			} else {
				$stack[] = $this->items[ $this->counter ];
				$this->items[ $this->counter ]->setMultilang();
			}
			$this->items[ $this->counter ]->setDefaultLanguage($defaultLanguage);
			$this->counter++;
		}

		$defaultInput->setSiblings($stack);

		return $defaultInput;
	}


	/**
	 * @param $name
	 * @param $label
	 * @param int $rows
	 * @param int $cols
	 *
	 * @return FormItemTextarea
	 */
	public function addTextarea($name, $label, $rows = 5, $cols = 30) {
		$this->isExists($name);
		$this->items[$this->counter] = new FormItemTextarea($name, $this->getLabelArray($label), $this->id, $rows, $cols);
		if($this->id->isStrict()) {
			if(version_compare(PHP_VERSION, '7.3.0') > 0) {
				$pattern = Form::PATTERN_ALPHANUMERIC_73;
			} else {
				$pattern = Form::PATTERN_ALPHANUMERIC;
			}
			$this->items[ $this->counter ]->setPattern( "Zadávajte iba alfanumerické znaky <strong>bez znakov _$!?|[]()*'\"+/</strong>",
				$pattern);
		}
		$this->defaultItemClass($this->items[$this->counter]);
		return $this->items[$this->counter++];
	}

	/**
	 * @param string $name
	 * @param string $label
	 *
	 * @return FormItemSubmit
	 */
    public function addSubmit($name = "submit", $label = " Odoslať ") {
        $this->items[$this->counter] = new FormItemSubmit($name,$label, $this->id);
        return $this->items[$this->counter++];
    }


	/**
	 * @param $name
	 * @param $label
	 * @param $items
	 *
	 * @return FormItemSelect
	 * @throws AbortException
	 */
    public function addSelect($name, $label, $items) {
	    $this->isExists($name);
        $this->items[$this->counter] = new FormItemSelect($name, $this->getLabelArray($label), $this->id, $items);
        $this->defaultItemClass($this->items[$this->counter]);
        return $this->items[$this->counter++];
    }

	/**
	 * @param $name
	 * @param $label
	 * @param $items
	 *
	 * @return FormItemRadio
	 * @throws AbortException
	 */
    public function addRadio($name, $label, $items) {
	    $this->isExists($name);
        $this->items[$this->counter] = new FormItemRadio($name, $this->getLabelArray($label), $this->id, $items);
        $this->defaultItemClass($this->items[$this->counter]);
        return $this->items[$this->counter++];
    }

	/**
	 * @param $name
	 * @param $value
	 *
	 * @return FormItemHidden
	 * @throws AbortException
	 */
    public function addHidden($name, $value = null) {
	    $this->isExists($name);
        $this->items[$this->counter] = new FormItemHidden($name, $value, $this->id);
        return $this->items[$this->counter++];
    }


	/**
	 * @deprecated
	 * @param $name
	 * @param $label
	 * @param $html
	 *
	 * @return null
	 */
    public function addHTML($name, $label, $html) {
	    throw new DeprecatedException("This FormItem is deprecated. Use latte rendering form.");
	    return null;
    }

	/**
	 * @param $html
	 * @deprecated
	 * @return mixed
	 */
    public function addPlainHTML($html) {
		throw new DeprecatedException("This FormItem is deprecated. Use latte rendering form.");
        return null;
    }

	/**
	 * @param $name
	 * @param $label
	 * @param array $list
	 *
	 * @return FormItemCheckbox
	 */
	public function addCheckbox($name,$label,array $list = []){
		$this->items[$this->counter] = new FormItemCheckbox($name,$label,$list,$this->id);

		return $this->items[$this->counter++];
	}

	/**
	 * @deprecated
	 * @param $label
	 *
	 * @return array
	 */
    private function getLabelArray($label) {
        if ($this->labelClass != "") {
            return array($label, $this->labelClass);
        }
        return $label;
    }

    private function defaultItemClass($object){
        if($this->itemClass != "") {
            $object->setAttr("class",$this->itemClass);
            return true;
        }
        return false;
    }

	public function setTitleElement($b){
		$e = explode(" ",$b);
		$el = reset($e);

		$this->titleStart = $b;
		$this->titleEnd = $el;
	}
	public function setItemElement($b){
		$e = explode(" ",$b);
		$el = reset($e);

		$this->itemStart = $b;
		$this->itemEnd = $el;
	}

	public function setBlockElement($b){
		$e = explode(" ",$b);
		$el = reset($e);

		$this->blockStart = $b;
		$this->blockEnd = $el;
	}
    public function setGroupElement($b) {
	    $e = explode(" ",$b);
	    $el = reset($e);

        $this->beforeElement = $b;
        $this->afterElement = $el;
    }

    public function setItemClass($c){
        $this->itemClass=$c;
    }

    public function setLabelClass($c) {
        $this->labelClass = $c;
    }

	/**
	 * Sets title of form block
	 * @param $title string
	 */
    public function setTitle($title){
        $this->title=$title;
    }
    /**
     * Vrati HTML kod bloku
     */
    public function draw() {
        $ret = "";
	    if($this->blockStart !== null and $this->blockEnd !==null){
		    $ret .= '<'.$this->blockStart.'>';
	    }
        if($this->title!=null and $this->wizard == false){
            $ret.= "<".$this->titleStart.">$this->title</".$this->titleEnd.">";
        }
	    /** @var $item FormItem */
	    foreach ($this->items as $item) {
            if ($item->render) {
	            if($this->beforeElement !== null and $this->afterElement!==null){
                    $ret .= '<'.$this->beforeElement.'>' . $item->getHTML($this->itemStart,$this->itemEnd) . '</'.$this->afterElement.'>';
	            } else {
		            $ret .= $item->getHTML($this->itemStart,$this->itemEnd);
	            }
            } else {
                $ret .= $item->getHTML($this->itemStart,$this->itemEnd);
            }
        }
	    if($this->blockStart !== null and $this->blockEnd !==null){
		    $ret .= '</'.$this->blockEnd.'>';
	    }

        return $ret;
    }

    public static function generateInput($item) {

        foreach (Language::fetchAll() as $lang) {
            if ($lang->id == Core::$conf["defaultLanguage"]) {
                $isActive = "button_lang_active";
                $style = "";
                $def = true;
            } else {
                $isActive = "";
                $style = 'style="display:none;"';
                $def = false;
            }
            //var_dump($item->getAttrs());
            $attr = $item->getAttrs();
            if (!$def) {
                if (strpos($item->getAttrs(), "required") !== false) {
                    $attr = str_replace("required=\"required\"", "", $item->getAttrs());
                }
            }
            $buttons.="<a href=\"#" . $item->getName() . "[$lang->id]\" class=\"button_lang $isActive\" $rel><img alt=\"$lang->id\" src=\"$lang->image\"></a>";
            $inputs .= '<input type="' . $item->getType() . '" id="' . $item->getFormID() . '-' . $item->getName() . '" name="' . $item->getName() . '[' . $lang->id . ']' .
                    '" value="' . $item->getValue($lang->id) . '" ' .
                    $attr . ' ' . $style . ' ' . $isActive . ' />';
        }

        $ret .= $inputs .
                '<div class="multilang_buttons">' .
                $buttons .
                '</div>';

        return $ret;
    }

    public static function generateTextArea($item) {
        $ret = "";

        foreach (Language::fetchAll() as $lang) {
            $rel = " rel=\"" . Core::$conf["defaultEditor"] . "\"";
            if ($lang->id == Core::$conf["defaultLanguage"]) {
                $isActive = "button_lang_active";
                $style = "";
            } else {
                $isActive = "";
                $style = 'style="display:none;"';
            }
            $buttons.="<a href=\"#" . $item->getFormID() . "-" . $item->getName() . "[$lang->id]\" aria-input=\"" . $item->getFormID() . '-' . $item->getName() . "-" . $lang->id . "\" class=\"button_lang $isActive\" $rel><img alt=\"$lang->id\" src=\"$lang->image\"></a>";
            $inputs .= '<textarea ' . $item->getAttrs() . ' id="' . $item->getFormID() . '-' . $item->getName() . '[' . $lang->id . ']" name="' . $item->getName() . '[' . $lang->id . ']' . '" ' . $style . ' ' . $isActive . '>' .
                    $item->getValue($lang->id) . '</textarea>';
        }

        $ret .= $inputs .
                '<div class="multilang_buttons">' .
                $buttons .
                '</div>';

        return $ret;
    }

}
