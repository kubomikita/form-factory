<?php


namespace Kubomikita;

use Kubomikita\Commerce\DatabaseService;
use Nette\Application\BadRequestException;
use Nette\Http\Request;
use Nette\Http\RequestFactory;
use Nette\Utils\Callback;
use Nette\Utils\Strings;

interface IFromAjaxHandler {
	public function __construct($get,$post=null,$files=null);
	public function handle();
	public function process();
	public function terminate();
}

class FormAjaxHandler implements IFromAjaxHandler{
	/**
	 * @var Form class
	 */
	protected $form = null;
	/*protected $form_id  = null;

	protected $_get = [];
	protected $_post = [];
	protected $_files = [];
	protected $_caller = [];
*/
	protected $formData = [];
	protected $componentData = [];

	protected $_request_type = null;
	protected $_function = null;
	protected $_method = null;
	protected $_generator = null;

	protected $errors = [];
	/** @var Request */
	protected static $httpRequest;



	public function __construct($get,$post=null,$files=null){
		\LangStr::$locale = isset($_SESSION["lang"]) ? $_SESSION["lang"] : \LangStr::$native;

		$httpRequest = $this->createRequest();


		if($this->checkMethod() && $this->checkAjax() && $this->checkParms()) {
			$this->formData = $this->getFormData();
			$this->componentData = $this->getComponentData();
			//$this->_function = $get["function"];
			//$this->_method = "_".strtolower($get["method"]);
			//$this->_generator = $get["gen"];

			//$this->_get   = $get;
			//$this->_post  = $post;
			//$this->_files = $files;
			//$this->_caller = $get["caller"];
	
			//$this->form_id = $this->{$this->_method}["formId"];
			//dump($get);

			//if(isset($get["type"])){
			if($httpRequest->getQuery("type") !== null){
				$this->_request_type = $get["type"];
			}
			if($httpRequest->getQuery("ajax_load") !== null /*isset($get["ajax_load"])*/){
				$this->_request_type = "content";
			}
			//dump($this);
			//bdump($this);
			//dumpe($this);
		}

	}

	/**
	 * @return Request
	 */
	protected function createRequest() : Request {
		$requestFactory = new RequestFactory();
		if(method_exists($requestFactory, "fromGlobals")){
			self::$httpRequest = $requestFactory->fromGlobals();
		} elseif (method_exists($requestFactory, "createHttpRequest")) {
			self::$httpRequest = $requestFactory->createHttpRequest();
		} else {
			self::$httpRequest = Service::getByType(Request::class);
		}
		return self::$httpRequest;
	}

	protected function getFormData() : array{
		if(($httpRequest = self::$httpRequest) instanceof Request){
			if($httpRequest->isMethod("POST")){
				return $httpRequest->getPost() + $httpRequest->getFiles();
			} elseif($httpRequest->isMethod("GET")){
				return $httpRequest->getQuery() + $httpRequest->getFiles();
			}
		}
		return [];
	}

	protected function getComponentData(): array {
		if(($httpRequest = self::$httpRequest) instanceof Request){
			$caller = $httpRequest->getQuery("caller");
			$componentData = [];

			if($caller === null || $caller["class"] == "undefined"/*["class"] == "undefined"*/ /*|| empty($this->_caller)*/) {
				$formName = isset($this->formData["formName"]) ? $this->formData["formName"] : $this->formData["formId"];
				$availableMethods = Form::getComponentName($formName);
				$e = explode("::",$this->_function);
				$componentData["class"] = $e[0];
				$componentData["function"] = $this->componentExists($e[0], $availableMethods);
				$componentData["name"] = $formName;
				//dump($e);
				return $componentData;
			}
		}
		return [];
	}

	protected function checkMethod() {
		$this->_method = self::$httpRequest->getMethod();
		//bdump(self::$httpRequest);
		return self::$httpRequest->getQuery("method") !== null && Strings::lower(self::$httpRequest->getQuery("method")) === Strings::lower(self::$httpRequest->getMethod());
	}

	protected function checkAjax() {
		return self::$httpRequest->isAjax();
	}

	protected function checkParms(){
		$this->_function = self::$httpRequest->getQuery("function");
		$this->_generator = self::$httpRequest->getQuery("gen");

		return $this->_function !== null && $this->_generator === FormFactory::GENERATOR;
	}

	protected function componentExists(string $objectName, array $components){
		if(class_exists($objectName)){
			$object = new $objectName;
			foreach ($components as $c) {
				if ( is_callable( [ $object, $c] ) ){
					return $c;
				}
			}
		}
		return null;
	}

	protected function initForm(){
		if(!empty($this->componentData)){
			if(isset($this->componentData["class"]) && isset($this->componentData["function"])){
				//if(class_exists($this->_caller["class"])) {
					$class = new $this->componentData["class"];
					if ( is_callable( [ $class, $this->componentData["function"] ] ) ) {
						$c = [ $class, $this->componentData["function"] ];
						$callback = $c($this->componentData["name"]);
						if ( $callback instanceof Form ) {
							return $callback;
						}
					}
				//}
			}
		}
		return null;
	}

	protected function handleForm(){


			$data = $this->formData;

			$ajax = $data["ajax"];
			$spam = $data["my_email"];
			$formId = $data["formId"];
			$formName = $data["formName"];
			unset($data["formId"], $data["ajax"], $data["my_email"], $data["formName"]);

			if($ajax and $spam == "nospam"){
				$this->form = $this->initForm();
				//dump($data, $this);
				if(!($this->form instanceof Form)) {
					$this->form = Form::getInstance( $formId );
					trigger_error("FORM $formId getting from \$_SESSION",E_USER_NOTICE);
				}

				$this->form->setIsAjax(true);
				$this->form->setValues($data /*+ $this->_files*/);
				$this->form->setRendered($data /*+ $this->_files*/);
				//bdump($this->form);
				if($this->form instanceof Form){
					try {
						echo $this->form->reactionToSuccess( $formId );
					} catch(\Exception $e){
						bdump($e);
						echo '<div class="alert alert-danger">'.$e->getMessage().'</div>';
					}
				}else{
					throw new \Exception("Cannot get parent class Form");
				}
			}
	}
	protected function handleContent(){
		if($this->_function !== null) {
			$data = $this->formData;
			$ajax = $data["ajax"];
			$spam = $data["my_email"];
			$formId = $data["formId"];
			//unset($data["formId"]);
			unset($data["ajax"]);
			unset($data["my_email"]);
			if($ajax and $spam == "nospam") {
				$this->form = $this->initForm();
				if(!($this->form instanceof Form)) {
					$this->form = Form::getInstance( $formId );
					//bdump($this);
					trigger_error("FORM $formId getting from \$_SESSION",E_USER_NOTICE);
				}

				//$this->form = Form::getInstance($formId);
				if($this->form instanceof Form) {
					$formItem = $this->form->getFormItemByName( $data["input_name"] );
					$this->form->checkFormItem( $formItem, [ $data["input_name"] => $data["value"] ] );
					$errors = $this->form->getErrors();
					if ( ! empty( $errors ) ) {
						$this->form->renderError( $errors );
					} else {
						if ( FormFactory::function_exists( $this->_function ) ) {
							call_user_func( $this->_function, $data, $this->form );
						}
						$this->form->resetErrors();
					}
				} else {
					dump($this);
				}

			}
		}
	}
	public function handle() {
		if($this->process()) {
			if ( $this->_request_type === null ) {
				$this->handleForm();
			} elseif ( $this->_request_type == "content" ) {
				$this->handleContent();
			}
			$this->terminate();
		}
	}
	public function terminate(){
		if($this->process()){
			if(class_exists("\\Kubomikita\\Service")) {
				$db = Service::get( "database" );
			}
			if(class_exists("\\Kubomikita\\Commerce\\DatabaseService")){
				DatabaseService::disconnect( $db );
			}
			exit;
		}
	}
	public function process(){
		return isset($this->_generator) && $this->_generator == FormFactory::GENERATOR;
	}

}