<?php


namespace Kubomikita;

interface IFormAlertHandler {
	public function handle(array $errors, $formId);
	public function getReset($formId);
}
class FormAlertHandler implements IFormAlertHandler{
	private $elementAppendStart = 'div class="invalid-feedback"';
	private $elementAppendEnd = "div";

	private $elementClass = "is-invalid";

	private $formId = null;
	private $err = [];

	public function handle(array $errors, $formId){
		$this->formId = $formId;
		$js = $err =  [];
		foreach($errors as $dom => $message){
			$e = explode("|",$dom);
			$domId = (isset($e[0])?$e[0]:"");
			$type = (isset($e[1])?$e[1]:null);
			$append = (isset($e[2])?$e[2]:null);
			if(is_numeric($domId) or trim($domId) == ""){
				$this->err[] = $message;
			} else {
				$handleJs = "js".ucfirst($type);
				if(!method_exists($this,$handleJs)) {
					$js[] = $this->jsDefault( $domId, $message, $append );
				} else {
					/** @method jsCheckbox */
					$js[] = $this->{$handleJs}($domId,$message);
				}

			}
		}
		$return  = implode("",$this->handleError());
		$return .= '<script>'.$this->reset($this->formId).' $(document).ready(function(){ '.implode(" ",$js).' });</script>';
		return $return;
	}

	private function jsDefault($domId,$message, $append = null){
		if(strlen(trim($append)) == 0) {
			
			return '$("#' . $domId . '").addClass("' . $this->elementClass . '").after(\'' . $this->getMessage( $message,
				$domId ) . '\');';
		} else{
			return '$("#' . $domId . '").addClass("' . $this->elementClass . '"); $("#'.$domId.'-append").after(\'' . $this->getMessage( $message, $domId ) . '\');'; // .after(\'' . $this->getMessage( $message, $domId ) . '\');
		}
	}
	private function jsCheckbox($domId,$message){
		return '$("#'.$domId.'").addClass("'.$this->elementClass.'").next("label").after(\''.$this->getMessage($message,$domId).'\');';
	}
	private function jsRadio($domId,$message){
		//$this->err[] = $message;
		//mp($domId);
		$r = '$("input[id^=\''.$domId.'\']").addClass("'.$this->elementClass.'");';
		//$r.= '$("input[id^=\''.$domId.'\']:last").parent("div.form-check").parent().append(\''.$this->getMessage($message,$domId).'\')';
		return $r;
	}
	public function getReset($formId){
		return '<script>'.$this->reset($formId).'</script>';
	}
	private function getMessage($message,$domId){
		return '<'.$this->elementAppendStart.' id="'.$this->getFeedbackId($domId).'">'.addslashes($message).'</'.$this->elementAppendEnd.'>';
	}
	private function reset($formId){
		return '
		$("#'.$formId.' input.'.$this->elementClass.',#'.$formId.' select.'.$this->elementClass.',#'.$formId.' textarea.'.$this->elementClass.'").each(function(){
			var elementId=$(this).attr("id");
			$(this).removeClass("'.$this->elementClass.'");
			//$("#feedback-"+elementId).remove();
		});
		$(".invalid-feedback").remove();
		';
		//return '$(';
	}
	private function getFeedbackId($domId){
		return 'feedback-'.$domId;
	}

	private function handleError($err = null){
		$show = [];
		$err = $this->err;
		if(!empty($err)){
			foreach($err as $e){
				$show[] = '<div class="alert alert-danger">'.$e.'</div>';
			}
		}
		return $show;
	}
}