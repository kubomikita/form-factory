<?php
namespace Kubomikita;
use Nette\Utils\Strings;

/**
 * Abstraktna trieda ako model pre komponenty formulara
 * 
 * @author Jakub Mikita <kubomikita@gmail.com>
 * @version 1.0
 */
abstract class FormItem {

    protected $name;
	protected $type;
    protected $label;
    /** @deprecated  */
    protected $labelClass;
    protected $labelAttrs = [];
    protected $value = null;//array();
    protected $attrs = array();
    protected $divAttrs =array();
    protected $required = null;
	/**
	 * @var Form class
	 */
    protected $form;
    protected $formObject;
    protected $multilang=false;
    protected $requiredLabel='<span class="form-required">*</span>';
    public $render=true;
    protected $desc;
    protected $ajax_object;
    protected $id;
    protected $pattern;
    protected $patternText;
	protected $dependsOn;
	protected $dependsOnValue;
	protected $equalTo;
	protected $equalToText;

	protected $itemStart,$itemEnd;

	protected $prepend = null;
	protected $append = null;
	protected $appendBtn = null;

	protected $translateAttr = ["placeholder"];
	protected $translateItems = true;
	protected $siblings = [];
	/**
	 * FormItem constructor.
	 *
	 * @param $name nazov html tagu
	 * @param $label popis html tagu
	 * @param $form Form class
	 */
    public function __construct($name, $label,Form $form) {

        $this->name = $name;
        $this->form = $form;

		if($label !== null && is_string($label)) {
			$this->setLabel( $label );
		}
        /** @deprecated  */
        if(is_array($label)){
            $this->setLabel($label[0]);
	        $this->setLabelAttr("class",$label[1]);
        }

	    $this->form->setSession("FormRequired",[]);
	    $this->form->setSession("FormEqualTo",[]);
	    $this->form->setSession("FormDependsOn",[]);
	    $this->form->setSession("FormPattern",[]);

    }

	/**
	 * @param bool $bool
	 *
	 * @return FormItem
	 */
    public function setTranslateItems(bool $bool) : self {
		$this->translateItems = $bool;
		return $this;
    }
	public function getErrorIdentifier(){
		return $this->getDomId()."|".$this->getType()."|".$this->getAppend();
	}
	public function getAppend(){
		//dump($this->append,(int)$this->append,(bool)$this->append,(int) (bool)$this->append);
		return (int) (bool)$this->append;
	}
    public function setDependsOn($name,$value = true){
        $this->dependsOn = $name;
	    $this->dependsOnValue = $value;
        $this->setAttr("data-depends",$name);
	    //$this->form->setSession("FormDependsOn",[$this->name=>$this->dependsOn]);
        return $this;
    }
	public function getDependsOn(){
		return $this->dependsOn;
	}
	public function checkDependsOn($val){
	    if(is_bool($this->dependsOnValue)) {
			if($this->dependsOnValue){
				return (bool) $val;
			} else {
				return ! (bool) $val;
			}
    	} elseif(is_string($val) || is_numeric($val)){
		    return $this->dependsOnValue == $val;
    	}
	}
	function getEqual(){
		if($this->equalTo !== null) {
			$text = $this->equalToText;
			if($text === null){
				$text = sprintf($this->form->getFormFactory()->getDefaultEqualText(),$this->equalTo,$this->getName());
				//$text = "Pole '$this->equalTo' nieje rovnaké ako pole '{$this->getName()}'.";
			}
			return [ $this->equalTo, $text ];
		}
		return [];
	}
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    public function setId($id){
        $this->id=$id;
        return $this;
    }
    public function setLabel($label) {
        $this->label = $this->form->getTranslator()->translate($label);
        return $this;
    }

    public function setValue($value) {
        $this->value = FormFactory::quote($value, $this->form->noescapeMode);
        return $this;
    }

    public function setAttr($name, $value) {
		if(in_array($name,$this->translateAttr)){
			$value = $this->form->getTranslator()->translate($value);
		}
        $this->attrs[$name] = $value;
        return $this;
    }

    public function setLabelAttr($name,$value){
	    if(in_array($name,$this->translateAttr)){
		    $value = $this->form->getTranslator()->translate($value);
	    }
	    $this->labelAttrs[$name] = $value;
	    return $this;
    }

	/**
	 * @deprecated
	 */
	public function setDivAttr(){

	}

	public function setItemElement($b){
		$e = explode(" ",$b);
		$el = reset($e);

		$this->itemStart = $b;
		$this->itemEnd = $el;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getSiblings(): array {
		return $this->siblings;
	}

	protected function getItemStart($s){
		if($this->itemStart!==null){
			$ret = $this->itemStart;
		} else {
			$ret = $s;
		}

		if(!($this->prepend === null) or !($this->append === null)) {
			if($ret !== null) {
				$re    = '/class=\"(.+)\"/';
				$subst = 'class="input-group $1"';
				$ret   = preg_replace( $re, $subst, $ret );
			}
			if($ret === "div") {
				$ret = 'div class="input-group"';
				$this->itemEnd = "div";
			}
		}

		return $ret;
	}
	protected function getItemEnd($s){
		if($this->itemEnd!==null){
			return $this->itemEnd;
		}
		return $s;
	}
    
    /**
     * Popis polozky. Je vhodne uviest, co sa ma do tejto polozky vlozit.
     * @param type $text
     * @return this
     */
    public function setDesc($text) {
        $this->desc = $this->form->getTranslator()->translate($text);
        return $this;
    }
    
    /**
     * Vrati hotovy HTML element popisu polozky
     * @return string
     */
    public function getDesc() {
        $ret ="";
        if($this->ajax_object instanceof FormAjaxEvent){
            $ret .= $this->ajax_object->getAjaxHtml();
        }
        if(strlen($this->desc)>0){
            $ret .= '<span class="help-block hidden-xs">';
            $ret .= $this->desc;
            $ret .= '</span>';
            
        }
        //var_dump($this->name,$this->formObject->getAjax());
        /*if($this->formObject->getAjax() and $this->type=="file"){
            $ret .= "progress bar";
        }*/
        return $ret;
    }
    /**
     * Prida k div atributu nieco
     * 
     * @param string $name
     * @param string $value
     * @return \FormItem
     */
    public function addDivAttr($name,$value){
        $this->divAttrs[$name] .= " ".$value;
        return $this;
    }
    public function setAjaxEvent($ajax_object){
        $this->ajax_object = $ajax_object;
        $this->ajax_object->setInputName($this->name);
        return $this;
    }
    public function getAjaxEvent(){
        return $this->ajax_object;
    }
    public function setRequired($text=null) {
        $this->required = $this->form->getTranslator()->translate($text);
	    if($this->required === null || strlen(trim($this->required)) === 0){
		    $this->required = sprintf($this->form->getFormFactory()->getDefaultRequiredText(),$this->label);
	    }
        $this->label=$this->label.' '.$this->requiredLabel;
	    $this->form->setSession("FormRequired",[$this->name=>$this->required]);
        return $this;
    }
	public function noStrict(){
		$this->removePattern();

		return $this;
	}
	public function removePattern(){
		$this->pattern = null;
		$this->patternText = null;
		$this->form->setSession("FormPattern",[$this->name=>[]]);
		return $this;
	}
    public function setPattern($text,$pattern){
    	if($this->form->getCaller() === null) {
		    $this->pattern = $pattern;

		    $this->patternText = $this->form->getTranslator()->translate( $text );
		    $this->form->setSession( "FormPattern",
			    [ $this->name => [ "patternText" => $this->patternText, "pattern" => $this->pattern ] ] );
	    }
        return $this;
    }
    public function setPatternOnly($pattern){
    	$this->pattern = $pattern;
	    $this->form->setSession("FormPattern",[$this->name=>["patternText"=>$this->patternText,"pattern"=>$this->pattern]]);
	    return $this;
    }
    public function getPattern(){
	    $ses = $this->form->getSession("FormPattern");
	    if(isset($ses[$this->name]["pattern"]) && $ses[$this->name]["pattern"] !== $this->pattern){
		    return $ses[$this->name]["pattern"];
	    }
        return $this->pattern;
    }
    public function getPatternText(){
	    $ses = $this->form->getSession("FormPattern");
	    if(isset($ses[$this->name]["patternText"]) && $ses[$this->name]["patternText"] !== $this->patternText){
		    return $ses[$this->name]["patternText"];
	    }
        return $this->patternText;
    }
    public static function checkPattern($pattern,$value){
        if($pattern != null){
            $pattern='/'.$pattern.'/u';
            $res=preg_match($pattern, $value,$arr);
            if(count($arr) > 0){
                return true;
            }
            return false;
        }
    }

    public function setRequired2($text,$pattern="") {
        $this->required = $text;
        $this->setAttr("required","required");
        $this->label=$this->label/*.' '.$this->requiredLabel*/;
        return $this;
    }
    
    public function getRequired() {
        return $this->required;
    }
    
    public function getName() {
        return $this->name;
    }
	public function getType(){
		if(isset($this->type)){
			return $this->type;
		}
		return null;
	}
    
    /**
     * 
     * @param type $lang ID jazyka, napr. 'sk' alebo 'en'
     * @return type
     */
    public function getValue($lang) {
        return $this->value[$lang];
    }
    
    public function getFormID() {
        return $this->form->getId();
    }

    private function processAttrArray($main,$arrays){
	    foreach ( $arrays as $array ) {
		    if(is_array($array)) {
			    foreach ( $array as $key => $value ) {
				    if ( isset( $main[ $key ] ) ) {
					    $main[ $key ] .= " " . $value;
				    } else {
					    $main[ $key ] = $value;
				    }
			    }
		    } elseif(is_string($array) && trim($array) != "") {
			    $args = explode("|",$array);
			    foreach($args as $a){
				    list($key,$value) = \Nette\Utils\Strings::split($a,'~\s*=>\s*~');
				    $value = \Nette\Utils\Strings::replace( $value, '~\"(.*)\"~', "$1" );
				    if ( isset( $main[ $key ] ) ) {
					    $main[ $key ] .= " ". $value;
				    } else {
					    $main[ $key ] = $value;
				    }
			    }
		    }
	    }

	    $ret = "";
	    foreach ($main as $attr => $value) {
		    $ret .= $attr.'="'.$value.'" ';
	    }
	    return $ret;
    }
    protected function processLabelAttr(){
	    $main = $this->getLabelAttrs();
	    $arrays = func_get_args();
	    return $this->processAttrArray($main,$arrays);
    }
	protected function processAttr(){
		$main = $this->getAttrs();
		$arrays = func_get_args();
		return $this->processAttrArray($main,$arrays);
	}

    public function getAttrs() {
        $ret = $this->attrs;

        if($this->ajax_object instanceof FormAjaxEvent){
            $ret += $this->ajax_object->getItemAttr();
        }

        return $ret;
    }

    public function getLabelAttrs(){
    	return $this->labelAttrs;
    }
    
    protected function getDivAttrs() {
        $ret = " ";
        foreach ($this->divAttrs as $attr => $value) {
            $ret .= $attr.'="'.$value.'" ';
        }
        return $ret;
    }

    public abstract function getHTML($itemStart=null,$itemEnd=null);
    
    public static function multilang() {}

	/**
	 * Return dom ID of form element
	 * @return string
	 */
	public function getDomId(){
		$name = $this->getHtmlId();
		if(strpos($name,"[]") !== false){
			$name = str_replace("[]","_array");
		}

		return /*$this->getFormID()."-".*/$name;
	}
	//public abstract function getControl($attrs=[]);
	public function getControl($attrs=[] /*,$itemStart=null,$itemEnd=null*/) {
		if($this->multilang){
			return;
		}
		if(!empty($this->siblings)){
			array_unshift($this->siblings, $this);
			$langs = $controls = [];
			ob_start();
			echo '<div class="multilang">';
			/** @var FormItemText $sibling */
			foreach ($this->siblings as $sibling){
				$match = Strings::match($sibling->getName(),'/[a-z]+-([a-z]{2})-[a-z_]+/m');
				$langs[$match[1]] = /*"container-".*/$sibling->getHtmlId();
				$siblingAttr = $attrs;
				if($match[1] !== $this->defaultLanguage){
					$siblingAttr += ["style" => "display:none"];
				}
				$controls[] = $sibling->renderControl($siblingAttr /*, $itemStart, $itemEnd*/);
			}
			echo '<div class="multilang_buttons">';
			foreach($langs as $lg => $htmlId){
				echo '<a href="javascript:;" data-lang-container="'.$htmlId.'" class="button_lang '.($lg === $this->defaultLanguage ? 'active' : '').'"><img alt="'.$lg.'" src="assets/img/flags/'.$lg.'.png"></a>';
			}
			echo '</div>';
			echo implode(" ", $controls);
			echo '</div>';
			$html = ob_get_clean();
			return $label . $html;
		}

		return $this->renderControl($attrs /*,$itemStart,$itemEnd*/ );
	}


	public abstract function getLabel($attrs=[]);
	public function getRawLabel(){
		if($this->label === null){
			return trim($this->getName());
		}
		return trim(str_replace($this->requiredLabel,"",$this->label));
	}
	protected function getItemValue(){
		if((isset($_POST[$this->name]) and $_POST["form_id"] == $this->getFormID())){
			return FormFactory::quote($_POST[$this->name], $this->form->noescapeMode);
		}
		return $this->value;
	}

	public function getHtmlId(){
		return $this->form->getId()."-".str_replace(".","-",$this->name);
	}
}

?>
