<?php
namespace Kubomikita;

class DefaultFormTranslator implements IFormTranslator {
	/**
	 * @var Form
	 */
	private $form;

	/**
	 * @param Form $form
	 *
	 * @return DefaultFormTranslator
	 */
	public function setForm(Form $form): self{
		$this->form = $form;
		return $this;
	}

	public function translate( $msgid ) {
		if(\LangStr::$locale != \LangStr::$native) {
			$msg = __($msgid);
			if($msg == $msgid) {
				//bdump( $msgid, "form [".$this->form->getId()."]" );
			}
			return $msg;
		}
		return $msgid;
	}
}