<?php
namespace Kubomikita;

class NullFormTranslator implements IFormTranslator {
	public function setForm( Form $form ) {
		return $this;
	}
	public function translate( $msgid ) {
		return $msgid;
	}
}