<?php

namespace Kubomikita;

interface IFormTranslator {
	/**
	 * @param string $msgid
	 *
	 * @return string
	 */
	public function translate($msgid);

	/**
	 * @param Form $form
	 *
	 * @return mixed
	 */
	public function setForm(Form $form);
}