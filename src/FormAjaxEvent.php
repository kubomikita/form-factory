<?php
namespace Kubomikita;
use Nette\InvalidStateException;

/**
 * Plugin pre FormFactory ktory umoznuje pridavat jednotlivím poliam vo Formulari Ajaxové eventy
 *
 * @author Jakub Mikita <kubomikita@gmail.com>
 * @version 1.0
 */

class FormAjaxEvent {

	protected $formId;
	/**
	 * @var Form
	 */
	protected $form;
	protected $event;
	protected $function;
	protected $responseDiv;
	protected $inputName;
	protected $inputId;
	protected $button;
	protected $buttonAttr=array();
	protected $checkAfter = 0;
	protected $noScroll = false;
	protected $loader;

	public function __construct($formId){
		if(!($formId instanceof Form)) {
			$this->formId = $formId;
			$this->form = Form::getInstance($formId);
		} else {
			$this->form = $formId;
			$this->formId = $formId->getId();
		}

		$this->responseDiv = $this->formId."_responseText";
		//$this->checkGeneratingId();
	}
	public function setInputName($name){
		$this->inputName=$name;
		//$this->checkGeneratingId();
		return $this;
	}
	public function setButton($button){
		$this->button = $button;
		return $this;
	}
	public function setFunction($func){
		$this->function=$func;
		if(!FormFactory::function_exists($func)){
			throw new InvalidStateException("AjaxEvent_function 'FormAjaxEvent': Success funkcia '$func' neexistuje");
		}
		return $this;
	}
	public function setCheckAfter($after){
		$this->checkAfter = $after;
		return $this;
	}
	public function setNoScroll($after){
		$this->noScroll = $after;
		return $this;
	}
	public function setLoader($element){
		$this->loader = $element;
		return $this;
	}
	public function setEvent($event){
		$this->event=$event;
		$this->stringifyEvent();
		return $this;
	}
	public function setResponseDiv($d){
		$this->responseDiv=$d;
		return $this;
	}
	public function checkGeneratingId(){
		if($this->formId != null and $this->inputName != null){
			$this->inputId = $this->formId."_".$this->inputName;
		}
	}
	private function stringifyEvent(){
		if(substr($this->event,0,2) != "on"){
			$this->event = "on".$this->event;
		}
		return $this->event;
	}
	public function setButtonAttr($attr,$value){
		$this->buttonAttr[$attr] = $value;
		return $this;
	}
	public function getButtonAttr(){
		$attrs="";
		foreach($this->buttonAttr as $attr => $value){
			$attrs.=' '.$attr.'="'.$value.'" ';
		}
		return $attrs;
	}
	public function getItemAttr(){
		$attr = [];
		$this->checkGeneratingId();
		if($this->button == null){
			$loader ="";
			if($this->loader !=""){
				$loader = ",'$this->loader'";
			}
			//dump($this->event,"javascript:kubomikitaAjaxLoad('$this->inputId','$this->function','$this->responseDiv',this.value,$this->checkAfter,".(int)$this->noScroll."".$loader.");");
			$attr[$this->event] = "javascript:kubomikitaAjaxLoad('$this->inputId','$this->function','$this->responseDiv',this.value,$this->checkAfter,".(int)$this->noScroll."".$loader.");";
		} else {
			$attr["onkeyup"] = "javascript:redirectText(this.value,'$this->inputId');";
			$attr["onchange"] = "javascript:redirectText(this.value,'$this->inputId');";
		}
		return $attr;
	}
	public function getAjaxHtml(){

	}
	public function getButton(){
		$this->checkGeneratingId();
		if($this->button!==null){
			return "<a id=\"CHECK_$this->inputId\" rel='' ".$this->getButtonAttr()." href=\"javascript:kubomikitaAjaxLoad('$this->inputId','$this->function','$this->responseDiv',getRelValue('$this->inputId'),$this->checkAfter,".(int)$this->noScroll.");\">$this->button</a>";
		}
		return null;
	}
}