
function kubomikitaLoadDependencies(){
	if(window.jQuery){
		window.jQuery().ajaxSubmit || document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"><\/script>');
	} else {
		window.setTimeout(function(){kubomikitaLoadDependencies();},50);
	}
}
function getFormLoader(){
	return "Nahrávam...";
}
function redirectText(a, b) { $("#CHECK_" + b).attr("rel", a) }

function getRelValue(a) {
    return $("#CHECK_" + a).attr("rel") }

function getValueFromId(a) {
    var b = $("#" + a);
    return b.attr("id") }

function kubomikitaAjaxLoad(a, b, c, d, e, f, g) {
    var h = $("#CHECK_" + a),
        i = h.html(),
        j = a.split("_"),
        k = $("#" + a.replace("_", "-")).attr("name");
		formId = $("#" + a.replace("_"+k,""));
		console.log(a);
	/*console.log(d);
	console.log(e);
	console.log(h);
	console.log(formId.data("generator"));*/
    d.length >= e && (h.attr("disabled", !0).html(getFormLoader()), $("#" + a.replace("_", "-")).parent().parent().find(".form-control").removeClass("is-invalid"), $("#feedback-" + a.replace("_","-")).remove(), "undefined" != typeof g && $(g).html('<div class="text-center">' + getFormLoader("black") + "</div>"), $.get(getPathFromUrl(document.URL), { my_email: "nospam", ajax: 1, method: "get", "function": b, value: d, formId: j[0], ajax_load: !0, input_name: k,gen: formId.data("generator") }, function(a) { "" != a ? (a.indexOf("<script") >= 0 && a.indexOf("<script") <= 15 ? $("#" + c).html(a).hide() : $("#" + c).html(a).show(), h.attr("disabled", !1).html(i), f || $("html, body").animate({ scrollTop: $("#" + c).offset().top - 50 }, 500)) : $("#" + c).hide() }))
}

function setInputClass(a, b, c) { "undefined" == typeof c && (c = "form-group");
    var d = $(a).parent().parent("." + c),
        e = $(a).prev("." + c);
    if (console.log(e), console.log(e.attr("class")), console.log(c), console.log(e.length), 0 != e.length) var d = e;
    if (0 == d.length) var d = $(a).parent().parent().parent("div");
    d.addClass(b)
}
function getBaseHref(){
	var base = document.querySelector('base');
	var baseUrl = getPathFromUrl(document.URL) || base && base.href;
	//return "/";
	return baseUrl;
}
function kubomikitaAjaxForm(a, b) {
	//$("#orderstep1_responseText").html(getBaseHref()+ " "+document.URL);
	var formid = $("#"+a);
	formid.data("ajaxized", 1);
    var c = getBaseHref() + "?function=" + formid.attr("data-func") + "&method=" + formid.attr("data-method")+"&gen="+formid.data("generator"); //+"&caller[class]="+formid.attr("data-caller-class")+"&caller[function]="+formid.attr("data-caller-function");
	//console.log(formid, c);
    if(formid.attr("data-caller-class") !== undefined){
    	c = c + "&caller[class]=" + formid.attr("data-caller-class");
    }
	if(formid.attr("data-caller-function") !== undefined){
		c = c + "&caller[function]=" + formid.attr("data-caller-function");
	}
    var d = $("#" + a + " button[type='submit']"),
        e = d.html();
    $("#progress_" + a + " .bar"), $("#progress_" + a + " .percent");
    var f = {
    	url: c,
	    beforeSubmit: function() {
    		var disabledText = typeof d.data("loader-text") !== "undefined" ? d.data("loader-text") : getFormLoader() + " Odosielam...";
    		d.attr("disabled", !0).html(disabledText);
		    $("#" + a + " .has-error").each(function() { $(this).removeClass("has-error") }), $("#" + a + "_responseText").hide() }, error: function(a, b, c) { console.log(b, c) }, success: function(c) {
            return "" != c ? 0 === c.indexOf("<link>") || 0 === c.indexOf("/") ? ($("#" + a + "_responseText").hide(), window.location.href = c.replace("<link>", "")) : ($("#" + a + "_responseText").html(c).show(), b && $("html, body").animate({ scrollTop: $("#" + a + "_responseText").offset().top - 50 }, 500), d.attr("disabled", !1).html(e)) : $("#" + a + "_responseText").hide(), !1 } };
    return $("#" + a).ajaxSubmit(f), !1
}
function getPathFromUrl(url) {
	return url.split("?")[0].split("#")[0];
}
function kubomikita_init(){
	kubomikita_nospam();
	/*$(document.body).find("form[data-ajax='1']").submit(function(e){
		e.preventDefault();
		kubomikitaAjaxForm($(this).attr("id"),$(this).data("ajax-scroll"));
	});*/

	$(document).find("form[data-ajax='1']").each(function (){
		$(this).off("submit");
		$(this).submit(function (e){
			e.preventDefault();
			kubomikitaAjaxForm($(this).attr("id"),$(this).data("ajax-scroll"));
		})
	})
	//console.log("FormFactory initialized!");
}
function kubomikita_nospam(){
	$(document.body).find("form[data-generator='commerce/form-factory']").each(function(){
		var form = this;
		var input = document.createElement("input");
		input.setAttribute("type","hidden");
		input.setAttribute("name","my_email");
		input.setAttribute("value","no"+"spam");
		form.appendChild(input);

	})
}

kubomikitaLoadDependencies();

$(document).ready(function(){
	kubomikita_init();
})