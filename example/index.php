<?php
session_start();
include __DIR__."/../vendor/autoload.php";
Tracy\Debugger::enable(false,__DIR__."/");

//session_regenerate_id();
//$_SESSION = [];
//dumpe("test",class_exists("LangStr"));

//try {
	$form = new Kubomikita\FormFactory();
	$form->setAjaxHandler(new Kubomikita\FormAjaxHandler($_GET,$_POST,$_FILES));
	$form->setAlertHandler(new \Kubomikita\FormAlertHandler());
	$form->setJavascriptFilename("js/form-factory.min.js");
	$form->register();


echo '<html><head>';
\Kubomikita\FormFactory::scripts();
	echo '<link rel="stylesheet" href="bootstrap.css">';
	//echo '<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">';
echo '</head><body><div class="container"><div class="row"><div class="col-lg-6">';

	/*$Form = new Kubomikita\Form();
	$Form->setOnSuccess("formSuccessAjax");
	$Form->setAjax();
	//$Form->setDebugMode();
	$Form->setStrict();

	//$Form->setClass("form-inline");

	$block = new Kubomikita\FormBlock($Form);
	$block->setTitle("first block");
	$block->setItemClass("form-control form-control-sm");





	$block->setGroupElement('div class="form-group"');

	$ajax_event = new Kubomikita\FormAjaxEvent($Form->getFormID());
	$ajax_event->setButton("Načítať")
			->setButtonAttr("class","btn btn-info")
			->setFunction("formGetIco")
			->setEvent("change")
			->setNoScroll(true);
	$block->addText("ico","IČO:")
			->setAjaxEvent($ajax_event)
			->addAppend($ajax_event->getButton(),true) // '<button onclick="javascript:return false;" class="btn btn-info">Overiť</button>'
			->setRequired("IČO musí byť vyplnené.")
			->setPattern("Nesprávny tvar IČO (músí mať 8 číslic)",Kubomikita\Form::PATTERN_ICO);
	$block->addText("dic","DIČ:");
	$block->addText("nazov","Názov spoločnosti:")
			->setRequired("Názov spoločnosti musí byť vyplnený.");
	$block->addText("sidlo","Sídlo spoločnosti:")
			->setRequired("Sídlo spoločnosti musí byť vyplnené.")
			->addPrepend('<span class="fa fa-home"></span>')->addAppend("test");


	$Form->addBlock($block);

	$block1= new Kubomikita\FormBlock($Form);
	$block1->setTitle("second block");
	$block1->setItemClass("form-control form-control-sm");
	$block1->setGroupElement('div class="form-group"');

	$block1->addText("email","Váš e-mail:")->setRequired("E-mail musí byť vyplnený.")->setPattern("Nesprávny tvar e-mailu",Kubomikita\Form::PATTERN_EMAIL);
	$block1->addText("email2","Váš e-mail, zopak.:")
		->setRequired()
		->setPattern("Nesprávny tvar e-mailu",Kubomikita\Form::PATTERN_EMAIL)
		->setEqualTo("email");
	$block1->addText("telefon","Váš telefón:")->addPrepend("+421");
	$block1->addCheckbox("vop","Súhlas so spracovaním osobných údajov.")->setRequired();
	$block1->addTextarea("text","Textarea pole");
	$Form->addBlock($block1);

	$b = new \Kubomikita\FormBlock($Form);
	$b->setTitle("Dodacia adresa");
	$b->setItemClass("form-control form-control-sm");
	$b->setGroupElement('div class="form-group"');

	$b->addCheckbox("d_on","dodacie su rovnake")->setAttr("checked","checked");
	$b->addText("d_meno","Meno")
		->setDependsOn("d_on",false)
		->setRequired("Dodacia adresa: Nevyplnili ste meno.");
	$b->addText("d_priezvisko","Priezvisko")
		->setDependsOn("d_on",false)
		->setRequired("Dodacia adresa: Nevyplnili ste priezvisko.");

	//$block2->addHidden("d_on","Dodacie")->setValue(0);
	$b->addSubmit("Uložiť");
	$Form->addBlock($b);

	//echo $Form->draw();*/
	echo "</div><div class='col-lg-6'>";

	$Form = new \Kubomikita\Form();
	//$Form->setDebugMode();
	//$Form->setOnSuccess("formSuccess");
	//$Form->setOnSuccess([FormSuccessModel::class,"post"]);
	$Form->setOnSuccess([new FormSuccessModel, "post"]);


	$Form->setAlertElement("div class=\"alert alert-danger\"");
	$Form->setAlertContainerElement('div');
	//$Form->addProtection();
	$Form->setAjax();

	$b = new \Kubomikita\FormBlock($Form);
	$b->setItemClass("form-control form-control-sm");
	$b->addText("username","Meno")->setRequired();
	$b->addText("password","Heslo")->setRequired();

	$b->addSubmit();
	$Form->addBlock($b);

	echo $Form->draw();

	$f = new FormSuccessModel();
	echo $f->createComponentForm()->draw();

	/*$Form = new \Kubomikita\Form();
	$Form->setOnSuccess("formSuccess");
	$Form->setAjax();
	$Form->setStrict();
	$Form->setDebugMode();

	$block  = new \Kubomikita\FormBlock($Form);
	$block->setItemClass("form-control");
	$block->addHidden("hiddeninput","1");
	$block->addText("ahoj","ahoj")->setRequired();
	$block->addCheckbox("suhlas","suhlasim!")->setRequired();
	$block->addTextarea("text","Textarea pole")->setRequired();
	$block->addSubmit("save");
	$Form->addBlock($block);
	$Form->register();*/

	echo '<hr>';
	/*echo $Form->start();
	echo '<div class="errors">'.$Form->errors().'</div>';
	echo '<div class="form-group row">
			<div class="col-4 col-form-label text-right">'.$Form->getLabel("ahoj").'</div>
			<div class="col-8">
				'.$Form->getControl("ahoj",["class"=>"KUBO"]).'
			</div>
		</div>
		<div class="form-group row">
			<div class="col-4 col-form-label text-right">'.$Form->getLabel("text").'</div>
			<div class="col-8">'.$Form->getControl("text").'</div>
		</div>
		<div class="form-check">'.$Form->getControl("suhlas").$Form->getLabel("suhlas").'</div>
		<br><br><br><br>
		<div class="text-right">'.$Form->getControl("save").'</div>';
	echo $Form->end();*/

	$latte = new Latte\Engine;
	//$latte->setTempDirectory(__DIR__.'/temp');
	\Kubomikita\Latte\Macro\FormFactoryMacro::install($latte->getCompiler());
	//$latte->render("form.latte");

	/*$f = new Kubomikita\Form();
	$f->setAjax();
	$f->setOnSuccess( "save" );
	$b = new Kubomikita\FormBlock( $f );
	$b->addText( "ahoj", "ahoj" )->setRequired();
	$b->addSubmit();
	$f->addBlock( $b );

	echo $f;*/


/*} catch(Exception $e){
	trigger_error($e->getMessage(),E_USER_ERROR);
}*/

bdump($_SESSION["kubomikita/form-factory"]["Forms"]);

class FormSuccessModel {
	public function onStartup(){
		bdump("startup");
		bdump($this);
	}

	public function createComponentForm(?string $name = null): \Kubomikita\Form{
		$Form = new \Kubomikita\Form($name);
		//$Form->setDebugMode();
		//$Form->setOnSuccess("formSuccess");
		//$Form->setOnSuccess([FormSuccessModel::class,"post"]);
		//$Form->setOnSuccess([new FormSuccessModel(), "post"]);


		$Form->setAlertElement("div class=\"alert alert-danger\"");
		$Form->setAlertContainerElement('div');
		//$Form->addProtection();
		$Form->setAjax();

		$b = new \Kubomikita\FormBlock($Form);
		$b->setItemClass("form-control form-control-sm");
		$b->addText("username","Meno")->setRequired();
		$b->addText("password","Heslo")->setRequired();

		$b->addSubmit();
		$Form->addBlock($b);

		$Form->onSuccess[] = function (\Kubomikita\Form $form, \Nette\Utils\ArrayHash $values){
			dump($values);
		};
		$Form->onSuccess[] = [new FormSuccessModel(),"post"];
		$Form->register();
		return $Form;
	}

	public static function post(\Kubomikita\Form $form, \Nette\Utils\ArrayHash $values){
		dump( $form , $values);
		echo "<div class='alert alert-success'>Formulár bol úspešne spracovanýsss.</div>";
	}
}


function save($form) {
	dump( $form );
}
function formSuccess(Kubomikita\Form $form){
	//$form->addError("Achoj");
	echo "<div class='alert alert-success'>Formulár bol úspešne spracovaný.</div>";
	//$form->redirect("?formSuccess=1");
}
function formSuccessAjax(Kubomikita\Form $form){
	//$form->addError("Nejaký problem stymto inputom.",$form->getItemId("nazov"));
	//$form->addError("Nejaký všeobecný error");

	echo "<div class='alert alert-success'>Formulár bol úspešne spracovaný.</div>";
	//$form->hide();
	//$form->redirect("?formAjaxSucces=1");
	//$form->refresh(5,"alert('danger');");
	dump($form);
}
function formGetIco($data){
	//dump($data);
	$formid = $data["formId"];
	if($data["value"] == "50305964"){
		echo '<script>$("#'.$formid.'-nazov").val("Jakub Mikita");$("#'.$formid.'-sidlo").val("Okružná 105");</script>';
	}
}

$cc = new ContactForm_controller();
$cc->onStartup();
dump($cc);

echo $cc->createComponentContactForm();//->draw();


echo '</div></div></div></body></html>';


use \Nette\Utils\Strings,
	\Nette\Reflection\ClassType,
	\Nette\Utils\Callback;

class ContactForm_controller{
	public $objName = 'ESHOP: Kontaktný formulár';
	public $component_init;
	public $components;
	public function onStartup(){
		$this->loadComponents();
	}
	private function loadComponents() : void{
		$c = ClassType::from($this);
		foreach($c->getMethods() as $method){
			if(Strings::contains($method->name,"createComponent")) {
				$key = Strings::firstLower(Strings::replace($method->name,"/createComponent/",""));
				$component = [$this,$method->name];
				$this->components[$key] = $component($key);
			}
		}
		$this->component_init = true;
	}

	public function getComponent(?string $key){
		if(!$this->component_init){
			$this->loadComponents();
			$this->component_init = true;
		}
		if(!isset($this->components[$key])){
			throw new \Nette\InvalidStateException("Component with name '$key' not exists in '".get_class($this)."'");
		}
		return $this->components[$key];
	}

	public function removeComponent(?string $key) : void{
		unset($this->components[$key]);
	}

	public function addComponent($component, string $name) :void {
		$this->components[$name] = $component;
	}

	public function createComponentContactForm(){
		$F=new Kubomikita\Form("contact");
		$F->setStrict();
		$F->setAjax(true);
		$F->setMethod("POST");
		$F->setOnSuccess("ContactForm_controller::contact");
		$b=new Kubomikita\FormBlock($F);
		$b->setGroupElement("div class='form-group'");
		$b->setItemClass("form-control");
		//$b->addHidden("to","");
		$b->addText("meno",("Vaše meno"));
		$b->addText("e-mail",("Váš e-mail"))->setPattern(("Neplatný e-mail"),Kubomikita\Form::PATTERN_EMAIL)->setRequired(("Nezadali ste Váš e-mail."));
		$b->addTextarea("text",("Otázka"))->noStrict()->setRequired(("Nezadali ste otázku."))->setAttr("style","min-height:130px;");
		$F->addBlock($b);
		$block3=new Kubomikita\FormBlock($F);
		$block3->addSubmit("submit",("Odoslať"));
		$F->addBlock($block3);
		$F->register();
		return $F;
	}

	public function actionContactForm(){
		$selected = $this->context->getService("request")->getQuery("to");
		$kontakt = Env::contact();
		$to      = [ "" => "-- vyberte --" ];
		$too = [];
		foreach ( $kontakt as $k ) {
			$to[ $k["email"] ] = $k["nazov"] . " - " . $k["email"];
			$too[$k["email"]] = $k;
		}
		if(!isset($to[$selected]) || $selected === null ){
			throw new \Nette\Application\BadRequestException();
		}
		$this->createComponentContactForm()->setDefaults(["to"=>$selected]);
		return ["contact" => $too[$selected]];
	}
	public static function contact(Kubomikita\Form $form){

		$data = $form->getValues(false);
		dumpe($data);
		//dump($data,$form);
		$to = $data["to"];
		$c = Env::contact();
		$cc = [];
		foreach ($c as $con){
			$cc[$con["email"]] = $con;
		}
		if(!isset($cc[$to])){
			$form->addError("Neznámy e-mail. (".$to.")");
		}
		$email = $data["e-mail"];
		$data = $form->getValues();

		//dump($data);
		unset($data["to"]);
		//dump($data);
		$valid = true;
		if($valid){
			$M=new Mail();
			$M->addTo($to);
			$M->addReplyTo($email);
			$M->setSubject("Kontakt z webu ".Registry::get("container")->getParameter("shop","name"));
			ob_start();
			foreach($data as $k=>$v){
				if($k!="formId"){
					echo '<strong>'.$k.':</strong> '.$v."<br>";
				}
			}
			$text = ob_get_clean();
			$template = Registry::get("latte")->renderToString(MAIL_DIR."message.latte",["message" => $text]);
			$M->setBody($template);
			//dump($M);
			$M->send();

			$response = new \Kubomikita\Form\Response\Shop();
			$response->setHideForm();
			$response->title = __("Formulár bol odoslaný");
			$response->message = __("Budeme sa snažiť, čo najskôr odpovedať na Vašu otázku...");
			$form->sendResponse($response);
		}
	}

};